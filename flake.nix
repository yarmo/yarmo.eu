{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/0a453b52727f8a42dce96a8152d5f2908c927b2f";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      {
        devShell = with pkgs; mkShell {
          buildInputs = [
            zola
          ];
        };
      });
}