+++
title = "Yarmo's contact"
+++

## Email

Feel free to send an email to [contact@yarmo.eu](mailto:contact@yarmo.eu) for correspondence, offers and opportunities that will enrich both our lives.

Reply otherwise not guaranteed.

## Encrypted communication platforms

We can communicate on privacy-friendly encrypted communication platforms. I'm available on Matrix and XMPP. Just have a look at my [Keyoxide profile](https://keyoxide.org/9F0048AC0B23301E1F77E994909F6BD6F80F485D) for contact details.

## Encrypted email

Not preferred but encrypted email communication is also a possibility.

If you are using an [OpenPGP-compatible email client](https://www.openpgp.org/software/), encrypt for the fingerprint `9f0048ac0b23301e1f77e994909f6bd6f80f485d`. The public key is available on the [homepage](/).

Otherwise, use my [tencrypt tool](https://tencrypt.org#hkp;9F0048AC0B23301E1F77E994909F6BD6F80F485D), write your message, click the `ENCRYPT MESSAGE` button and send me the result of that encryption.