+++
title = "Yarmo's travels"
description = ""
weight = 20

template = "travels-section.html"
page_template = "travels-page.html"

sort_by = "date"

paginate_by = 0
paginate_path = "page"
paginate_reversed = false

generate_feed = true

[extra]
+++