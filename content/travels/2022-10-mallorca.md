+++
title = "Mallorca"
date = "2022-10-01"

[extra]
flag = "🇪🇸"
header_image = "https://assets.yarmo.eu/!qDt5dpS8VC"
+++

<div class="map map--01"></div>
<script>
    const el = document.querySelector('.map--01');
    const map = L.map(el).setView([39.63, 2.9], 9);
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);
    L.polyline([
        [39.57, 2.65],
        [39.795, 2.696],
        [39.907, 3.084],
    ], {
        color: '#454f54',
        opacity: 0.6
    }).addTo(map);
    L.marker([39.57, 2.65], {
        title: "Palma",
        opacity: 0.6
    }).addTo(map);
    L.marker([39.795, 2.696], {
        title: "Port de Sóller",
        opacity: 0.6
    }).addTo(map);
    L.marker([39.907, 3.084], {
        title: "Port de Pollença",
        opacity: 0.6
    }).addTo(map);
</script>

I made a short trip to Mallorca to visit a friend I hadn't seen in nine years. They were busy during the day and I had a big deadline to meet, so there wasn't as much traveling around as one would hope for, but still managed to see some lovely sights.

The weather was mostly cloudy with some rain now and then. But I'm not complaining :)

## Palma

{{ instant(url="https://assets.yarmo.eu/!aGg95S9WEq", caption="Port of Palma") }}

{{ instant(url="https://assets.yarmo.eu/!3bGnwpAzAx", caption="Outside the city walls") }}

{{ instant(url="https://assets.yarmo.eu/!Xcv2ye4FVs", caption="Llotja de Palma") }}

{{ instant(url="https://assets.yarmo.eu/!Mwf4c3cqt3", caption="Llac del Cigne") }}

{{ instant(url="https://assets.yarmo.eu/!UP6s7dmrr3", caption="Cathedral of Palma") }}

{{ instant(url="https://assets.yarmo.eu/!qDt5dpS8VC", caption="Cathedral of Palma") }}

{{ instant(url="https://assets.yarmo.eu/!25AwdV5JXg", caption="Beach at predusk") }}

## The port of Sóller

{{ instant(url="https://assets.yarmo.eu/!QkJBghKxgi", caption="Port of Sóller") }}

## On the way to Pollença

{{ instant(url="https://assets.yarmo.eu/!JqnKKym4sQ", caption="Lake in front of hill") }}

## The port of Pollença

{{ instant(url="https://assets.yarmo.eu/!cRkpHVSskB", caption="Port of Pollença") }}
