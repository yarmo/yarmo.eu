+++
title = "Yarmo's now"
+++

- Recently started working as a software engineer for [NLnet](https://nlnet.nl)
- Getting back into music production, exploring synthesis and microtonality
- Working on Keyoxide in my spare time