+++
title = "Yarmo's blogroll"

aliases = ["friends"]
+++

[Aral Balkan](https://ar.al/)  
[Cendyne](https://cendyne.dev/)  
[DeavidSedice](https://deavid.wordpress.com/)  
[Derek Sivers](https://sive.rs/)  
[Garrit](https://garrit.xyz/)  
[Guillermo Garron](https://www.garron.blog/posts/)  
[Lopeztel](https://lopeztel.xyz/)  
[Karan Goel](https://goel.io/)  
[Kev Quirk](https://kevq.uk/)  
[Manuel Moreale](https://manuelmoreale.com/)  
[Mike Stone](https://mikestone.me/)  
[Nathan DeGruchy](https://degruchy.org/)  
[Soatok (Dhole Moments)](https://soatok.blog/)  