+++
title = "Views and opinions expressed are mine"
slug = "01"
weight = 1
+++

All views and opinions expressed are mine unless stated otherwise.

They do not necessarily reflect the official policy or position of any organization or company I work for or am otherwisely associated with.

Any information provided by me is true to the best of my knowledge.

Any content provided by me is not intended to malign any religion, ethnic group, club, organization, company, individual or anyone or anything.