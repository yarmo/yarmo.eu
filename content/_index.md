+++
title = "Yarmo"

template = "index.html"
page_template = "page.html"
+++

<div class="h-card">

<img class="u-photo" src="/avatar.png" />

I am <span class="p-given-name">Yarmo</span>.  
My pronouns are <em>
<span class="p-x-pronoun-nominative">he</span>/<span
class="p-x-pronoun-oblique">him</span>/<span
class="p-x-pronoun-possessive">his</span></em>.

I'm a <span class="p-note"><span class="p-category">FOSS</span> advocate</span>
and a <span class="p-note">Rust/NodeJS/web programmer</span>.
I am the <span class="p-note">developer of
<span class="p-org">[Keyoxide](https://keyoxide.org)</span></span> and
<span class="p-note">finished a PhD project in
<span class="p-category">Neurosciences</span></span>.
I also [make music](https://faircamp.yarmo.eu/).

[/now](/now) ·
[/uses](/uses) ·
[/reads](/reads) ·
[/watches](/watches) ·
[/vinyl](/vinyl) ·
[/blogroll](/blogroll) ·
[/cv](/cv)

## Tenets

- Less but better
- [Public money? Public code!](https://publiccode.eu/)

## Highlighted projects

- [Keyoxide](https://keyoxide.org) · Online identity verification using cryptography
- [delightful.club](https://delightful.club) · Curated lists of Free and Open Source software
- [EDOmame](https://edo.yarmo.eu) · An exploration in microtonality

## Quick links

- Email: <a href="mailto:contact@yarmo.eu" rel="me" class="u-email">contact@yarmo.eu</a>
- Personal website: <a href="https://yarmo.eu" rel="me" class="u-url u-uid">yarmo.eu</a>
- Keyoxide: <a href="https://keyoxide.org/aspe:keyoxide.org:TOICV3SYXNJP7E4P5AOK5DHW44" rel="me">my profile</a>
- Fediverse: <a href="https://fosstodon.org/@yarmo" rel="me">yarmo@fosstodon.org</a>
- Code repositories: <a href="https://codeberg.org/yarmo" rel="me">yarmo@codeberg.org</a> · <a href="https://github.com/yarmom" rel="me">yarmom@github.com</a>

## Cryptography

OpenPGP fingerprint: `9F0048AC0B23301E1F77E994909F6BD6F80F485D`  
OpenPGP key: <a href="https://yarmo.eu/9F0048AC0B23301E1F77E994909F6BD6F80F485D.asc" rel="pgpkey authn" class="u-key">armored key</a> · 
[binary key](https://yarmo.eu/9F0048AC0B23301E1F77E994909F6BD6F80F485D.pgp)

## Publications

Mackenbach Y, Borst JG (2023) Somatic Integration of Incoherent Dendritic Inputs in the Gerbil Medial Superior Olive [[link](https://www.jneurosci.org/content/43/22/4093.abstract)] [[PDF](/mackenbach-2023-dzw-mso.pdf)]

</div>