+++
title = "Music"
weight = 200
+++

## Music

### Own music

I released my first EP in 2023.

Listen on: [listen.yarmo.eu](https://listen.yarmo.eu)  

### Sir Jupiter

Together with my two brothers, I have created music since 2008 as the band called Sir Jupiter and have played a number of concerts in France and the Netherlands. We built our own home recording studio where we recorded and released two albums and two EPs.

To celebrate the 10th anniversary of our eponymous first album, we remixed and remastered it, even revisited some unused takes and recorded a few new takes.

Website: [sirjupiter.com](https://sirjupiter.com)  
Listen on: [listen.sirjupiter.com](https://listen.sirjupiter.com)  