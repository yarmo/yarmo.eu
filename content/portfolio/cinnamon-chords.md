+++
title = "Cinnamon Chords (software)"
weight = 2
+++

## Cinnamon Chords (software)

Side project that uses a static site generator to build an alternative to popular
websites that display the lyrics and chords of songs.

### Technologies used

Zola · static site generator written in Rust  

### Code repositories

**cinnamon-chords** · My static chords instance  
[website](https://cinnamon.yarmo.eu/), [source code](https://codeberg.org/yarmo/cinnamon-chords)

**static-chords** · Zola theme for lyrics and chords repositories  
[source code](https://codeberg.org/yarmo/static-chords)