+++
title = "Keyoxide (software)"
weight = 0
+++

## Keyoxide (software)

*Online identity verification using cryptography*.

I started the Keyoxide project in August of 2020. As a weekend side project, I built an online tool for message encryption and decryption using OpenPGP cryptography. This got turned into a platform for decentralized identity verification using OpenPGP keys.

Since 2020, I have received two grants from the [NLnet foundation](https://nlnet.nl/) for the development of Keyoxide.

In 2023, I gave a [lightning talk](https://archive.fosdem.org/2023/schedule/event/keyoxide/) about the Keyoxide project at the FOSDEM conference in Brussels.

### Technologies used

Web (HTML, JS, CSS) · various web clients, landing pages  
NodeJS · Keyoxide server, proxy server, CLI, developer libraries  
Rust · profile exchange server, CLI, developer libraries  
Dart · developer library  

### Code repositories

**keyoxide project** · Verifying online identity with cryptography   
[list of repos](https://codeberg.org/keyoxide)

Some notable repos:

**keyoxide-web** · Keyoxide web interface  
[website](https://keyoxide.org), [source code](https://codeberg.org/keyoxide)

**doip.js** · Node/JS library to interact with decentralized online identities  
[source code](https://codeberg.org/keyoxide/doipjs)

**doip.rs** · Rust library to interact with decentralized online identities  
[source code](https://codeberg.org/keyoxide/doip-rs)

**tencrypt** · Terse encryption tool using OpenPGP for the web  
[website](https://tencrypt.org), [source code](https://codeberg.org/yarmo/tencrypt)

**aspe-server-rs** · ASP Exchange server (Rust)  
[source code](https://codeberg.org/keyoxide/aspe-server-rs)

**kx-aspe-cli** · Keyoxide profile generator CLI using ASPE (Rust)  
[source code](https://codeberg.org/keyoxide/kx-aspe-cli)

**aspe-rs** · ASP Exchange client library (Rust)  
[source code](https://codeberg.org/keyoxide/aspe-rs)

**aspe-dart** · ASP Exchange client library (Dart)  
[source code](https://codeberg.org/keyoxide/aspe-dart)