+++
title = "Academic research"
weight = 100
+++

## Academic research

### Publications

Mackenbach Y, Borst JG (2023) Somatic Integration of Incoherent Dendritic Inputs in the Gerbil Medial Superior Olive [[link](https://www.jneurosci.org/content/43/22/4093.abstract)] [[PDF](/mackenbach-2023-dzw-mso.pdf)]

Busquets-Garcia A, Marsicano G (2017) Pregnenolone blocks cannabinoid-induced acute psychotic-like states in mice [[link](https://www.nature.com/articles/mp20174)]

### PhD project in Rotterdam (2015-2019)

In my PhD project, I studied the relationship between input and output of individual Medial Superior Olive cells responsible for the localization of sound. These cells are among the fastest neurons in the brain, operating with a temporal resolution high enough to detect sub-millisecond time differences. Combining a custom novel auditory stimulus and spectral/circular statistics analysis tools programmed specifically for this stimulus, we investigate how dendritic signals interact and generate spikes.

It was my role to lead the project, design the experiments and perform them, analyse the data, write the results and conclusions and present the findings at conferences in the Netherlands and overseas.
