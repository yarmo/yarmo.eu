+++
title = "Yarmo's portfolio"

template = "portfolio-section.html"
page_template = "portfolio-page.html"

sort_by = "weight"

generate_feed = false
+++