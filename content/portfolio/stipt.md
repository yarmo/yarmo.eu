+++
title = "Stipt (software)"
weight = 1
+++

## Stipt (software)

*The Sequential Time Planning app*

Side project that lets people manage their time as a sequence of appointments.

Designed for health workers that visit their patients at home, but applicable in
a variety of scenarios.

Enables the user to easily swap appointments, manage delayed appointments and
see their impact on the day's planning.

### Technologies used

Web (HTML, CSS) · web interface  
Typescript · app logic  
Appwrite · backend-as-a-service  

### Code repositories

**stipt-web** · Stipt web interface  
[website](https://stipt.yarmo.eu), [source code](https://codeberg.org/yarmo/stipt-web)