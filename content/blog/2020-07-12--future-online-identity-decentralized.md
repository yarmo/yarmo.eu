+++
title = "The Future of Online Identity is Decentralized"
slug = "future-online-identity-decentralized"
date = "2020-07-12 16:23:31"

aliases = ["post/future-online-identity-decentralized"]

[taxonomies]
tags = ["cryptography", "identity", "decentralization"]

[extra]
discussion = [
  "https://lobste.rs/s/ckalve/future_online_identity_is_decentralized",
  "https://tild.es/qj0",
  "https://news.ycombinator.com/item?id=23811568"
]
+++

## Online identity

[Online&nbsp;identity](https://en.wikipedia.org/wiki/Online_identity) refers to the concept of "being" in the digital world. As an internet user, you exist. You create accounts on websites. You write on social media and blogs. You post photos. All this online activity has in common that one and the same person performed these actions; it defines your "online&nbsp;identity".

However, your "online&nbsp;identity" is not *per se* representative of your "social&nbsp;identity" in the physical world.

You may choose to use your own name or a pseudonym. You may choose to publish personally identifiable information or not. You may choose to remain truthful to your social identity or deceive. In short, you may choose for authenticity or for anonymity.

## Authenticity versus anonymity

Authenticity and anonymity aren't mutually exclusive and that is the beauty of the internet. In the physical realm, you are (mostly) limited to a single social identity. In the digital space, there are no such restrictions. While you can't embody multiple persons in the offline world, you can have several identities online. In fact, you can even have multiple accounts on the same platform, opting for a different balance between authenticity and anonymity for each one of them.

The anonymity has its downsides, creating psychological artifacts like [online&nbsp;disinhibition](https://en.wikipedia.org/wiki/Online_disinhibition_effect) and facilitating [cyberharassment](https://en.wikipedia.org/wiki/Cyberbullying). However, even though we are far from completely overcoming these challenges, the internet that allows us to remain anonymous is still the one we should want and fight for.

## Consolidation of identity and internet corporations

Removing the possibility for anonymity could solve the problem of online toxicity. Large internet corporations like Google and Facebook allow all to create an account on condition that some personally identifiable information is revealed, usually a phone number.

The benefit is that it deters most from repeatably creating new accounts when older accounts have been flagged or banned due to improper behavior. These companies gain the function of "identity provider": they manage your online identity that can be used to login in different locations of the internet. We all know many websites that offer a "Google login" or "Facebook login".

But there is a problem: handling the entire online identity of a single person is too much responsibility for any corporation or organization, especially if it is in their interest to gain intimate individual knowledge and sell it (Google) or use it to manipulate moods and influence decision making (Facebook).

That phone number that was once used to prevent online toxicity is now the first of many pieces of personally identifiable information that these corporations will seek and use to figure out who you are.

"You have nothing to hide"? Great. The internet corporations will still make money hand over fist by selling your personality, your preferences, your buying patterns and your vote. And not just yours. That of entire populations.

Know that profits are just the tip of the iceberg. Governments all around the world are also interested in knowing what their citizens think, say and do for very different motives.

## Going decentralized

The solution is relatively simple. When you create a new account and get to choose between "Google&nbsp;login", "Facebook&nbsp;login" and "Email&nbsp;login", pick "Email&nbsp;login".

The benefit of not giving away any more personal data and tracking possibilities outweigh the inconvenience of having to fill in your email address and a password, especially when using a password manager. As tempting as the alternative is, making these changes will improve your life and ultimately, when enough people join these efforts, that of the world population.

A different problem arises: how to prove online identity when decentralized?

## Decentralized online identity

When you are no longer relying on an identity provider to manage your entire online identity, you lose the one common thing all your accounts on different platforms had: if two accounts on different online platforms are created by the same Google or Facebook account, we can safely assume they belong to the same person.

But this "trust by proxy" is lost when the accounts on those platforms were created without identity provider. And whether authentic or anonymous, it can sometimes be extremely useful to know and trust that separate accounts on the internet belong to the same person, even when not knowing who this person is.

The username is not sufficient to identify accounts across platforms. If you are "Alice" on one website, chances are you might need to be "Alice123" on the next one. And what if someone close to you is contacted by an "Aliss" requesting an amount of money to be transferred because they believe you to be in some sort of trouble? A poor attempt at impersonation, I know&hellip; Don't worry, a real bad actor will put in more effort and make a much more convincing act.

## Proving decentralized online identity

What if not only your online identity is decentralized, but also the tool to prove said online identity? This would mean that you wouldn't need to depend on a single company or entity to prove your identity across platforms. Decentralized identity, decentralized proofs!

Such solutions are already being deployed in industry, for example by firms like [Indicio.tech](https://indicio.tech/) which focus on blockchain technology.

Built for individuals, I recently launched [Keyoxide](https://keyoxide.org) which uses cryptographic keypairs to accomplish decentralized identity verification. While it doesn't (and shouldn't!) link an account to a person in the physical realm, it links accounts across platforms.

If you trust an account on one platform, you can trust any other account on any other platform as long as they are both verified by "identity proofs" stored in the same keypair. Whether you choose authenticity or anonymity, decentralized identity proofs allow you to build a cross-platform online identity.

Here's my [Keyoxide profile](https://keyoxide.org/9f0048ac0b23301e1f77e994909f6bd6f80f485d). In this case, I link to several "authentic" accounts but I could easily generate a new keypair void of personal data that links to several anonymous accounts. The accounts don't need to be authentic to create an online persona.

All the accounts listed in the link above belong to me. No one else could claim these accounts. Here's how.

## Identity proofs

An "identity proof" is nothing more than a link to an account A on some platform P stored inside your keypair K. If a "proof verification tool" such as Keyoxide follows this link and discovers some piece of data linking back to keypair K (which is only possible if keypair K and account A on platform P belong to the same person), the account is verified. If this proof verification is done for several accounts on different platforms, it is beyond reasonable doubt that the same person owns said accounts.

No bad actor could claim one of your accounts: the piece of data that links back is specific to your keypair, not the bad actor's keypair. And the bad actor also couldn't insert a proof inside your keypair as long as your keypair isn't compromised. Only you, the owner of the keypair, can add new proofs. But the entire world can read and verify them.

These identity proofs are decentralized because Keyoxide doesn't store them, your cryptographic keypair does. Keyoxide simply reads the keys and verifies the proofs. When you remove a proof from your keypair, Keyoxide will no longer have access to it. You own your proofs and your online identity.

In fact, the proofs are readable by everyone and are not specifically designed for Keyoxide. Anyone can use any tool or create new ones to verify these proofs and developers are encouraged to enrich this field with additional tools and services. Let's build a decentralized identity ecosystem we can all trust.

## Online identity beyond today's internet

Initiatives like [Solid](https://inrupt.com/solid) by [Sir Tim Berners-Lee](https://en.wikipedia.org/wiki/Tim_Berners-Lee) are paving the way for a new internet where all data is owned by the user and shared with platforms with consent and restrictions. This would solve the online identity problem: you get the benefits of a "pseudo centralized" account while maintaining full ownership over all account-related data stored on a decentralized platform. Social media would be allowed to see some data, messaging platforms some other data. But there would still be one single account to rule all the platforms.

On today's internet, the best we can do is make fully separated accounts, link them using technologies like decentralized online identity proofs and create our own online personas, with our own open tools that ensure we maintain ownership over them.
