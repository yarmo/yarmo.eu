+++
title = "LunaSea: FOSS FTW"
slug = "lunasea"
date = "2020-05-22 19:47:38"

aliases = ["post/lunasea"]

[taxonomies]
tags = ["short", "nuggets", "selfhosting"]
+++

`#100DaysToOffload >> 2020-05-22 >> 023/100`

## Out with the old

A couple of weeks ago, I finally discovered a FOSS alternative for nzb360, a great app for managing Plex, Radarr, Sonarr, etc. I wish to have kept using nzb360, but unfortunately, the app relies too heavily on Google Services and though I have paid for it, I can no longer use it as my LineageOS phone can't process purchases made on official Google Android phones.

## In with the new

Named [LunaSea](https://www.lunasea.app), it can do anything it should, (manage Sonarr, Radarr, Lidarr, NZB clients), it looks fantastic, it's available for both Google Android and iPhone and, of course, it's [FOSS](https://github.com/LunaSeaApp/LunaSea).

Only thing I'm missing is a donation button. And a fediverse account :)