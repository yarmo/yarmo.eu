+++
title = "Github is sinking"
slug = "github-sinking"
date = "2020-06-29 13:06:56"

aliases = ["post/github-sinking"]

[taxonomies]
tags = ["short", "gafam"]
+++

*If you're looking for a more reasoned argumentation, see Update 3 at the bottom.*

I rarely interact with [Github](https://github.com) anymore. All my projects are either on my selfhosted [Gitea](https://gitea.io) instance or on [Codeberg.org](https://codeberg.org/). That's why I missed the following on [Github Status](https://www.githubstatus.com/):

![Github status shows a lot of downtimes](/content/img/github_status.png)  
*Yikes*

Yikes, indeed. How everyone handles this is up to them. Large projects will find it hard to move, no doubt.

My interpretation? The Microsoft Github ship is sinking and it's sinking faster every day. The beauty is: you don't need them. Instead of relying on Github, you could:

- selfhost your own [Gitea](https://gitea.io) instance if you have the knowledge;
- use [Codeberg.org](https://codeberg.org/) which also uses [Gitea](https://gitea.io);
- use [sourcehut.org](https://sourcehut.org/) which takes a different but very solid approach to git hosting;
- use any instance generously hosted by amazing people (think [libreho.st](https://libreho.st/) and [Chatons](https://chatons.org/));
- use [gitlab.com](https://gitlab.com/) or selfhost an instance.

There are so many better places to be for git hosting nowadays. For an easy performance comparison of different services, see [forgeperf.org](https://forgeperf.org/).

Abandon the corporate ship before or after it sinks, up to you.

---

## Update 1

Added [forgeperf.org](https://forgeperf.org/) link after suggestion by [@slow@mstdn.io](https://mstdn.io/@slow).

---

## Update 2

Added [sourcehut.org](https://sourcehut.org/) link after suggestion by [@freddyym@social.privacytools.io](https://social.privacytools.io/@freddyym).

---

## Update 3

Don't publish on your website when you are feeling frustrated; that's what Twitter is for.

Let's inject some reason here. Github isn't dying anytime soon. Certainly not due to this number of outages. And all software breaks, so that's no measure; what matters is the response. And Github is on it. Like every single other time it was broken.

But that doesn't mean we can't change the status quo. Almost every defense of Github comes down to discoverability: if I put my project on Github, others will find it. If I put it elsewhere, other won't find it.

Do not forget: Github's discoverability comes from us, the userbase. We the developers make or break Github. If we all move, Github shuts its doors. This won't happen. But look at the landscape: so many alternative solutions exist, Github is no better than any other service and, in the eyes of some, me included, Github may actually provide a worse experience than most alternatives.

And about discoverability. Have you heard of social media? Blog posts? I discover a lot of new Github projects on a regular basis and almost none, I have discovered via Github itself. People talk about good projects and share them, plain and simple.

If you simply like Github and their network and their continuously "evolving" UI, have at it. To each their own.

If you don't like Github, do not stay. Be the change you want to see.
