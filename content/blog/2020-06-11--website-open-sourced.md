+++
title = "Website is now open source!"
slug = "website-open-sourced"
date = "2020-06-11 16:09:40"

aliases = ["post/website-open-sourced"]

[taxonomies]
tags = ["short", "meta"]
+++

It's finally here: the [source code of this website](https://git.yarmo.eu/yarmo/yarmo.eu) on my selfhosted gitea instance. It was delayed because, even though the current codebase does not contain secret keys or passwords, this has been the case in the past and the git history is easily searchable. I have deleted the old git project and started afresh.

From now on, the source code and the [drone CI/CD pipelines](https://drone.yarmo.eu/yarmo/yarmo.eu/) that go with the website are all open and available. This should make the content on the website more trustworthy as you can now review the code that generated the content. It is also my belief that the open-sourcing of this website is beneficial to all including myself: it gives you a chance to see the inner workings and perhaps pick up a trick or two, and if you see a blatant mistake, bad coding practices or other errors, I trust you will [let me know](/contact).

Enjoy and thanks for taking the time to be here :)
