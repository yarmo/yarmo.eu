+++
title = "How does a textbook 'Embrace, Extend, Extinguish' operation work?"
slug = "textbook-eee"
date = "2020-05-27 13:22:21"

aliases = ["post/textbook-eee"]

[taxonomies]
tags = ["gafam"]
+++

I recently found out about what happened to the [AppGet](https://appget.net/) tool for Windows made by [Keivan Beigi](https://keivan.io).

Sadly, a [recent blog post](https://keivan.io/the-day-appget-died/) is outlining the details around the decision to cease development and shut down the service which provided an open source package manager to Windows.

Stories about open source services shutting down are always sad and a blow to the community, but this one in particular is noteworthy. Judging from the events as written down by Keivan in his [post](https://keivan.io/the-day-appget-died/), he has been the target of an absolute textbook case of [Embrace, Extend, Extinguish](https://en.wikipedia.org/wiki/Embrace,_extend,_and_extinguish), Microsoft's _modus operandi_.

As a disclaimer, I have been rooting this past year for Microsoft's apparent change in stance towards Linux and the open source community, but I was wrong, and Microsoft has been kind enough to provide all the grounds to distrust the corporation even more, recently with the [MAUI debacle](https://itsfoss.com/microsoft-maui-kde-row/) (see all the [comments marked off-topic in this issue](https://github.com/dotnet/maui/issues/35)? Flagrant censoring!) and now with AppGet.

So now, let's quickly examine Microsoft's emails from the blog post.

## Embrace

> Keivan,<br>
> I run the Windows App Model engineering team and in particular the app deployment team. Just wanted to drop you a quick note to ***thank you for building appget*** — it’s a great addition to the Windows ecosystem and makes Windows developers life so much easier. We will likely be up in Vancouver in the coming weeks for meetings with other companies but if you had time we’d love to meet up with you and your team to get feedback on how we can make your life easier building appget.

**Embrace**: celebrate what people contribute to your ecosystem

## Extend

> Keivan,<br>
> it was a pleasure to meet you and to find out more about appget. I’m following up on the azure startup pricing for you. As you know we are big fans of package managers on Windows and ***we are looking to do more in that space***. My team is growing and part of that is to build a team who is responsible for ensuring package managers and software distribution on Windows makes a big step forward. ***We are looking to make some significant changes*** to the way that we enable software distribution on Windows and there’s a great opportunity (well I would say that wouldn’t I?) to help define the future of Windows and app distribution throughout Azure/Microsoft 365.<br>
> With that in mind ***have you considered spending more time dedicated to appget and potentially at Microsoft***?

**Extend**: get a foothold in people's successful contributions to your ecosystem

## Extinguish

> Hi Keivan, I hope you and your family are doing well — BC seems to have a good handle on covid compared to the us.<br>
> I’m sorry that the pm position didn’t work out. I wanted to take the time to tell you how much we appreciated your input and insights. ***We have been building the windows package manager*** and the first preview will go live tomorrow at build. We give appget a call out in our blog post too since ***we believe there will be space for different package managers on windows***. You will see our package manager is based on GitHub too but obviously with our own implementation etc. our package manager will be open source too so ***obviously we would welcome any contribution from you***.<br>
> I look forward to talking to you about our package manager once we go live tomorrow. Obviously this is confidential until tomorrow morning so please keep this to yourself. You and chocolatey are the only folks we have told about this in advance.

**Extinguish**: replace the people's contributions with your own products; it needn't be better because you're a big rich corporation with enormous reach

## Microsoft Loves Linux

Make no mistake: this aggressive pattern will continue. They like the name MAUI? They take it and silence the critics. They want a package manager because Linux has them? They "get inspired", build a new one and squash the existing solutions.

They love Linux, right? They are certainly "embracing" it on their platform when they launched the Windows Subsystem for Linux (or WSL), a tool to run Linux distributions inside Windows. It has also been a while since they started "extending" Linux and the open source community by means of [open-sourcing Powershell](https://itsfoss.com/microsoft-open-sources-powershell/) and [acquiring Github](https://itsfoss.com/microsoft-github/). Soon, WSL2 will launch with their [own Linux kernel](https://github.com/microsoft/WSL2-Linux-Kernel).

Now is the time to remain vigilant but also act. Donate to or support in any other way your favorite distribution and open source tools. Microsoft is coming.

## Final notes

I've tried to remain respectful to the content Keivan has posted in his [blog post](https://keivan.io/the-day-appget-died/). I feel sorry for the situation: he's the developer hero Windows needed but Microsoft felt we did not deserve. Hope you'll go on to make even bigger projects, Keivan, because you absolutely nailed AppGet!
