+++
title = "InfluxDB 2 migration"
slug = "influxdb-2-migration"
date = "2022-05-20 18:36:41"

[taxonomies]
tags = ["selfhosting", "homelab", "monitoring"]
+++

## What is InfluxDB?

[InfluxDB](https://www.influxdata.com/) is the [MIT licensed](https://github.com/influxdata/influxdb/blob/master/LICENSE) [time series database](https://en.wikipedia.org/wiki/Time_series_database) of my monitoring stack of choice, the so-called [TICK stack](https://www.influxdata.com/blog/introduction-to-influxdatas-influxdb-and-tick-stack/), consisting of [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/) (data collection agent), [InfluxDB](https://www.influxdata.com/) (time series database), [Chronograf](https://www.influxdata.com/time-series-platform/chronograf/) (charts and dashboard interface) and [Kapacitor](https://www.influxdata.com/time-series-platform/kapacitor/) (data processing engine).

## My history with InfluxDB

I started my homelab somewhere in 2017-2018. When about a year in I faced some random crashes and reboots which I did not manage to understand or fix, I first turned to [netdata](https://github.com/netdata/netdata) before settling with the TICK stack.

While I did eventually solve the random crashes, an obvious issue surfaced: if I want to monitor a crashing server, I probably shouldn't host the monitoring stack on that same server.

And so, my setup eventually converged to the one I use today: a homelab and a few VPSs, all monitored by a TICK stack installed on a dedicated VPS.

[InfluxDB 2.0](https://docs.influxdata.com/influxdb/v2.0/reference/release-notes/influxdb/) was released in November of 2020 and I remember this moment clearly: I updated my InfluxDB docker container, I noticed everything immediately broke, I read up on what a gargantuan update this 2.0 release was, I said "nope" and immediately reverted to 1.8.x.

Yesterday, I decided it was finally time to sit down and calmly move to InfluxDB 2(.2.0).

## InfluxDB 2, or the Death of the TICK stack

Please refer [release notes for InfluxDB 2](https://docs.influxdata.com/influxdb/v2.0/reference/release-notes/influxdb/) to see for yourself what has changed.

One of the most notable changes is that InfluxDB 2 now has a interface with graphs and dashboards (replacing Chronograf's functionality) and can send alerts based on data (which previously required Kapacitor). While I am sure Chronograf and Kapacitor can still be used together with InfluxDB 2, they no longer need to and indeed, I have now removed these services from my monitoring stack.

## InfluxDB 2, the migration

The [InfluxDB 2 dockerhub page](https://hub.docker.com/_/influxdb) has clear instructions on how to migrate from 1.x to 2.x when using containers and so I did. And while the container logs showed the migration process was successful, I had this weird issue where when I would try to then log in on the web interface, I was greeted with a "fresh install" screen, suggesting the InfluxDB instance was in fact not aware of the migration.

And indeed, after following the instructions of the "fresh install" screen, everything was working fine but no trace of any migrated data :(

If my historical InfluxDB data was in any way valuable to me, I would have put more effort into migrating all of it to InfluxDB 2.

But it wasn't.

## InfluxDB 2, the non-migration

And thus, in a bit of an anticlimax to this whole situation, I `rm -rf`ed the whole data directory and started anew with a fresh InfluxDB 2 instance. No more Chronograf, no more Kapacitor. All servers have updated Telegraf configurations and I am currently rebuilding the dashboards in InfluxDB's web interface.

And I also still need to rebuild my Grafana dashboards to make use of Flux instead of InfluxQL. I still have things to do before I can fully enjoy my monitoring solution again.