+++
title = "Flipper Zero and their \"Limited\" pledges"
slug = "flipper-zero-limited"
date = "2020-07-31 11:13:30"

aliases = ["post/flipper-zero-limited"]

[taxonomies]
tags = ["short", "rants"]
+++

## The Flipper Zero project

I'm not going to lie, Flipper Zero sounds like a cool project for hackers. Here's a [link to their website](https://flipperzero.one/zero) which will lead you to their Kickstarter page.

## What is going on on Kickstarter?

Something extremely scummy is going on right now! Have a look:

![Flipper Zero Kickstarter](/img/blog/kickstarted_counter__1a.png)

Looking good, lot's of stuff to read, let's take our time.

![Flipper Zero Kickstarter](/img/blog/kickstarted_counter__1b.png)

My word, they're almost out of Early Birds! Please, for the love of god, if you want to save some money, pledge now, only 9 left and it clearly says "Limited"!

### One minute later

![Flipper Zero Kickstarter](/img/blog/kickstarted_counter__2.png)

A person has just pledged! Where's my credit card?

### Another minute later

![Flipper Zero Kickstarter](/img/blog/kickstarted_counter__3.png)

Wait, 9 left? Someone bailed? Doesn't matter, I need this!

### Yet another minute later

![Flipper Zero Kickstarter](/img/blog/kickstarted_counter__4.png)

Wait, what?

### And it goes on

![Flipper Zero Kickstarter](/img/blog/kickstarted_counter__5.png)

### And on

![Flipper Zero Kickstarter](/img/blog/kickstarted_counter__6.png)

## This needs to stop

Well, you get the point. Flipper Zero is having some employee continuously adding more "Limited" pledges to perpetually give the impression they are almost out of "Early Bird" kits.

That's extremely deceptive and manipulative behavior and should not be tolerated. This needs to stop right now.
