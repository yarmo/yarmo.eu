+++
title = "No to .io, yes to .xyz!"
slug = "no-io-yes-xyz"
date = "2020-06-18 13:09:19"

aliases = ["post/no-io-yes-xyz"]

[taxonomies]
tags = ["rants"]

[extra]
discussion = [
  "https://lobste.rs/s/a6bbpf/no_io_yes_xyz",
  "https://tildes.net/~tech/pus/no_to_io_yes_to_xyz",
  "https://news.ycombinator.com/item?id=23562107",
  "https://www.reddit.com/r/programming/comments/hbdtfy/no_to_io_yes_to_xyz/"
]
+++

> TL;DR: I openly urge all FOSS projects and startups to reconsider registering .io ccTLD domains, opting instead for truly generic TLDs like .xyz

**.io** is dead, long live **.xyz**!

*UPDATE: Long live .xyz, .org, and many other gTLDs! Please see [Update 2](#update-2) below.*

Is that an exaggerated statement? Yes, yes it is. But all new projects (and startups?) should reconsider their choice of TLD.

## The case in favor of **.io**

The *de-facto* choice is [.io](https://en.wikipedia.org/wiki/.io). Numerous startups use it as a way to make their offering more legitimate, due to the long history of it being used by businesses, starting in [1998 with levi.io, registered by Levi Strauss & co](https://en.wikipedia.org/wiki/.io#History). The appeal comes from the shortness of the TLD and, with regards to the high-tech sector, it being the abbreviation for "input/output".

## The case against **.io**

But it doesn't mean "input/output". It stands for [British Indian Ocean Territory](https://en.wikipedia.org/wiki/British_Indian_Ocean_Territory) as it is indeed a ccTLD (i.e. country-specific) and not a generic gTLD.

Look at the [logo on the wikipedia page](https://en.wikipedia.org/wiki/.io). Looks techy, right? Everyone knows what the intended use was ("entities connected with British Indian Ocean Territory"), what the actual use is ("startup companies and browser games; little if anything related to the territory itself") and are happy to play along because $$$.

**.io** is one of the most expensive TLDs out there (overview on [domaincompare.io](https://www.domaincompare.io/) and no, the irony is not lost on me ^\_^). Stating the obvious, this is not due to the British Indian Ocean Territory having become such a hot property over the last decade.

The tech industry has appropriated the **.io** ccTLD and everyone is cashing in on it. Everyone?

## Colonial history and **.io**

In 2014, Gigaom reported in two separate articles ([article 1](https://gigaom.com/2014/06/30/the-dark-side-of-io-how-the-u-k-is-making-web-domain-profits-from-a-shady-cold-war-land-deal/), [article 2](https://gigaom.com/2014/07/11/uk-government-denies-receiving-io-domain-profits/)) what shady practices happen behind the scenes of the **.io** TLD management. Afraid of not doing the story any justice with my words, I ask you to read both articles and make up your own opinion on the matter.

The first one describes how the UK gets profits for **.io** while denying any claims from the [Chagossians, the people native to the Chagos Islands](https://en.wikipedia.org/wiki/Chagossians) whom they [expelled from the islands](https://en.wikipedia.org/wiki/Expulsion_of_the_Chagossians) (a matter which is [still actual in 2020!](https://en.wikipedia.org/wiki/Expulsion_of_the_Chagossians#2018_ICJ_hearing)). The second article describes how, in response to the first article, the UK government denied receiving profits and therefore defended that no profits should be shared with the Chagossians.

In january of 2019, [The Guardian wrote](https://www.theguardian.com/world/2020/jan/05/uk-forfeit-security-council-chagos-islands-dispute):

> Last February the ***International Court of Justice (ICJ)***, the principal judicial body of the United Nations, issued an advisory opinion that ***found the UK was in unlawful occupation of the islands*** and demanded that they be returned to Mauritius as quickly as possible.
>
> The ***UN general assembly endorsed the opinion*** in May and set a deadline for implementation of 22 November 2019, which the ***UK ignored***.

One may not agree with me, but it is my interpretation that, since the Chagossians aren't seeing any profits for the ccTLD that corresponds to the land they lived on but were forcibly removed from, **by buying .io domains, one directly supports the still-actual behavior of the UK government defending their colonial history and acts against human rights**.

## FOSS and **.io**

Why do we make FOSS software? Because we believe in openness and equality. It doesn't matter who you are, you can use my software, you can modify it, you can redistribute it.

Everything that has happened with the Chagossians and the **.io** TLD is in stark opposition to the core principles of the FOSS community.

## The case in favor of .xyz

The [.xyz TLD](https://en.wikipedia.org/wiki/.xyz) is fun, small, refreshing, funky, a whole lot cheaper and you don't support colonialism.

## Final words

If you choose to make your projects FOSS, you choose to uphold and respect certain principles and human rights, such as the [Four Essential Freedoms of Free Software](https://en.wikipedia.org/wiki/Free_and_open-source_software#Four_essential_freedoms_of_Free_Software).

It is my opinion that buying a **.io** TLD domain directly opposes all a FOSS developer stands for.

I openly urge all FOSS projects and startups to reconsider registering **.io** ccTLD domains, opting instead for truly generic TLDs like **.xyz**.

## Disclaimer

I have bought .io domains in the past. I did not have the knowledge of what was going behind the **.io** TLD. Now that I do, I will let them expire and NOT renew them. I will also never buy a ccTLD again if its use exceeds the intended use, namely to represent the territory it is associated with.

---

## Update 1 {#update-1}

There is also the issue of the **.io** TLD's [future](https://www.prolificlondon.co.uk/marketing-tech-news/tech-news/2019/05/future-popular-io-domains-question-over-british-empire-row):

> But the UK faces significant international pressure over the Islands, and in the event that they are returned, control over the .io TLD would likely pass to the Mauritian government.

Who knows what will happen to your domain registration when control is passed to the Mauritian government? Why risk the future of your domain just so you can associate your brand and/or product with the words "input/output"?

---

## Update 2 {#update-2}

It has been pointed out by many that this post focuses too much on the **.xyz** gTLD. This was not my intention. In fact, any gTLD will do just fine, after all they are generic. A non-exhaustive list of gTLDs that could perfectly replace **.io** (assuming **.io** simply stands for "input/output"):

- .net
- .org
- .tech
- .site
- .link
- .systems
- .computer

What is important is that you can identify with the TLD, be it **.net** or even **.ooo**.

I would also like to point out **.io** is not the only ccTLD that is often "misused" (IMO) as a gTLD. Think of **.ai**, **.tv**, **.to** and **.ly** to name just a few. The reason I single out **.io** is because this one in particular has a lot of controversy around it that has lasted for forty years and is still active. I haven't found the same levels of conflict with the other ccTLDs. If there are, do let me know, I read all links to discussion below.
