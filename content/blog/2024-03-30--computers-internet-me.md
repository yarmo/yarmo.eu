+++
title = "Computers + internet + me != three"
slug = "computers-internet-me"
date = "2024-03-30 20:39:59"

[taxonomies]
tags = []
+++

It has been a bit since my last blog post. This hiatus was partly due to my evolving stance on what computers and the internet mean to me, and to what degree I let them into my life.

I have some decisions to make, and so I thought it would be a good time to slow down for a moment and take inventory of my current thoughts on the subject, learn from where I come from and attempt to extract some insights helpful for the years to come.

## 2015-2020: The academia years

I had to use computers a lot during my PhD, mostly for data analysis. And my research generated quite the amount of raw data. Luckily, there were plenty of non-computer activities to provide some semblance of balance.

In my personal life, I used computers a fair bit, mostly gaming and in the later years, running a homelab. I was not really involved in FOSS yet, though I was making some small websites at that time for fun. My non-computer activities included time-consuming hobbies like making music, though as the PhD wore on, most hobbies disappeared, and so did my music.

I did spend plenty of time on the internet. I imagine YouTube must have been big for me, I probably was still using Facebook in the early years, though I had stopped using that for sure by 2019, likely 2018 and probably 2017. I never used Twitter. With some shame, I must admit I was a bit of a Google-believer in those days so for sure, I was spending a fair amount of time on G+.

I made my main Fediverse account in April of 2019 so by that time, I must have quit all Big Tech social media anyway.

## 2020-2024: The FOSS years

Dealing with a post-PhD burnout and the lockdowns was an interesting time for me. Both started practically at the same time.

A few months in, I began Keyoxide, a FOSS project which has been my fulltime activity from that point on until about two months ago. In terms of work, I was now spending 100% of my time behind a computer.

I barely had hobbies during these years. A big distraction was gaming with my brothers, which meant more computer time. I also did some game speedrunning. Once again, more computers. I also had a homelab.

These years must also have been my most active from a social media perspective. Of course, it was 100% Fediverse. I tried out various services but microblogging had been my main outlet.

From about 2022, I did experience a shift in my stance towards computers: I started reading books a lot more and I also doubled down on my maintaining journals, a habit I began in 2018.

## Computers and me

Computers have been a big part of my life during the past decade. I needed them to do research, and then I needed them as I worked on my FOSS project. I needed them to play games. I needed them to record and produce music.

I have never been a big social media person, but I did invest a fair amount of time on the Fediverse.

In other words, I have spent a significant amount of my time behind screens. It is actually more confrontational to realize this than I would have expected. 2023 was the year in which I truly started to slowly awaken to this painful fact.

The last few months, I have been discovering the joys of writing with various instruments like the dip pen. I have been toying around with watercolour painting. I have been experimenting with music, building a modular synthesizer (with an analog-only soundpath, no digital chips!) and exploring microtonality.

Looking back at 2023, I can easily spot the moments and activities I remember with greater fondness: they were the ones that did not involve computers.

## The good and the bad

I had great moments with computers. Maintaining a FOSS project has allowed me to meet fabulous, passionate people and build valuable friendships. I have been able to give a few small talks for which I am grateful. I have been able to stay in close contact with my brothers in darker times thanks to gaming, with a level of closeness that we might not have achieved otherwise.

I also had frustrating moments with computers. Research was stressful and resulted in a burnout. The homelab was not always the most fun to maintain. Doing FOSS fulltime has led to some of the most stressful moments in my life. Having to use computers for music production is a sure way to kill some of the inspiration and motivation.

Internet-wise, the story is similar. My FOSS project wouldn't have existed without the internet. I have no positive things to say about my Big Tech social media days, but my Fediverse days were a mixed bag.

Once again, I was able to talk with amazing people and have interactions I will remember for the rest of my life. But the Fediverse is also deeply flawed. I will never forget the following paraphrased interaction with a Fediverse developer in what must have been 2021 or so:

- me: you're making a new app, great, I would like to ask you to include mental health features (timers, etc) and avoid addictive dark patterns (endless scrolling, etc)
- them: \[insert big tech social media app here\] does it this way, so I will too
- me: big tech makes their services as addictive as possible because money, there is no monetization on the Fediverse so you can make better apps, if you copy big tech apps you'll end up getting people addicted for no good reason
- them: I won't be making money from this personally so I am fully justified to make addictive apps

That day marked the beginning of my slow but steady decline of activity on the Fediverse. Despite being one of the "better" places on the internet I have experienced, decently removed from Surveillance Capitalism, the Fediverse might well turn out to be just a different face of the same shitty coin.

I had hoped to be wrong, but 2024 is proving me right so far: a certain short-form video app has been widely documented to be extremely addictive and detrimental to the attention span of entire generations. So what is the Fediverse doing? Copying the platform 1:1 and claiming the moral high ground for not taking advantage of the addictions they will be causing.

## Key insights

Computers are tools. As much as I have a great appreciation for computers, how they let me accomplish my wildest dreams, they are nothing more than tools. It's foolish to spend more time with them than strictly necessary.

The internet relies entirely on computers. Ergo, the internet is a tool.

## Applying these insights

I was able to make a big change in the early months of 2024: I got a job. As a software engineer, yes. But two months in and already my perspective on many things is changing: having a steady income means I can view computers even more as just a tool, one that I can drop when it's *me time*. For the first time in years, I truly have *me time* again.

As such, I will focus on myself the coming time. And there will be no computers involved. I need no computers for watercolour painting or writing when I have a dip pen and a typewriter. I am actively exploring the process of producing entire songs, from the recordings to the final mixdown, entirely on my mixing desk.

And I have some other little projects in the pipeline, which need experimenting and exploring. In other words, they need my brain lucid, undistracted by computers or the internet.

The Fediverse is not for me. I am glad it exists and thrives but its apparent shortsightedness still makes me uncomfortable. The internet is decent at storing and distributing knowledge and sucks at most other things, thus I'll be directing my internet activity accordingly. I might get back to blogging a bit more. I have a few websites focused on sharing knowledge to maintain. Other than that, the internet currently has no larger appeal to me.
