+++
title = "Search engine indexing: DDG vs Google"
slug = "search-engine-indexing"
date = "2020-05-06 10:10:45"

aliases = ["post/search-engine-indexing"]

[taxonomies]
tags = ["short"]
+++

`#100DaysToOffload >> 2020-05-06 >> 012/100`

Having my own website means I get to control what happens on a tiny tiny part of the internet; it's my space. More importantly, I want to have a bit of control about what people see when they decide to put my name in a search engine. This is an important reason to have a website in the first place: I don't believe anyone would want their Facebook page to be their first impression, or anything the search engine decides to put first.

Months ago, I did a little test, searched my name in both Google and DuckDuckGo, didn't see my website which I just started, didn't think too much of it and went on with my life. Yesterday, I checked again. Let's compare the experiences.

Without any of my input, DuckDuckGo had found my website and it's the first thing anyone sees when searching for my name: mission accomplished. On Google, my website was not on the first page. Or the second. Or the third. After looking around in their "Webmaster Tools", I found out they had never figured out my website existed. I had to manually request the indexing which they say will be done at some point. Couldn't request an indexing without a good ol' game of finding crosswalks in a never-ending series of small images presented in a 3x3 grid.

In your opinion, what is the better experience?