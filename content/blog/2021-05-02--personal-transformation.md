+++
title = "A personal transformation"
slug = "personal-transformation"
date = "2021-05-02 15:19:55"

aliases = ["post/personal-transformation"]

[taxonomies]
tags = ["personal-development"]
+++

I'd like to share a few insights I gained during the last few months.

## What happened

First, a bit of background. For a while now, I have been feeling unproductive and unsure how to improve the situation.

That's it, really. Whether it's adapting to a post-academia lifestyle or learning to work from home, I knew I was doing
something wrong. I could spend days waiting for my head to clear up, then start working again to finally notice after
a few days that my head was foggy once more.

The frustrating part was that I knew how to be productive. For a little over four years, academia pushed me up to (and 
beyond) the limit of my mental capacity for working.

I knew the post-academia recovery was still ongoing, but I felt that didn't explain the whole picture. I lacked
understanding of what was going on, so I started making a few changes of my own and educating myself. The Dunning-Kruger
effect was not going to keep me in my state of ignorance.

## My own changes

I picked up intensive bullet journaling again. While this saved my mental health during my PhD, it did little for my
post-PhD predicament. Apparently, the problem was deeper than simply freeing my head from the burden of remembering
stuff.

I picked up more hobbies but found myself more often feeling guilty of giving in to those hobbies rather than spending
time on what I wanted to do: being productive.

It was also during this phase of exploring possible actions that I formulated a wish: I wanted to go abroad for a month
or two to a quiet location, take nothing but a bunch of clothes and my Thinkpad X201i and just start hammering away on
the keys. To my frustration, I could not provide arguments as to why I was yearning for this. I got no further than
"fully reset my lifestyle, go back to basic, build a new method and take those lessons back home". It sounds reasonable
but, well, knowing what I know now&hellip;

I scouted for a few locations where I already knew some people but a wish it remained. The pandemic. Safety above all.

And in the midst of fearing my situation was lightyears away from improving, I stumbled upon two little words on some
small personal blog, a drop of wisdom easily overlooked in a vast knowledge-overloaded internet.

## Educating myself

I had started reading a few books but so far none had resonated with me&mdash;none, until that one book. I was reading a blog post when I got puzzled by a couple of words (paraphrasing because I lost the link to the post):

> [...] compatible with the concept of **deep work**, [...]

Thank all the deities for being it precisely those two words that my brain decided to fixate on! A quick non-google
search later and I found the book the words were referring to:
**Deep Work: Rules for Focused Success in a Distracted World** by Cal Newport. A DRM-free purchase later and I started
my read.

It was after barely a few pages that I decided to not go the same route as with the other books by reading a chapter
every day and steadily progress through the book&mdash;I am generally a very slow reader. No, instinct told me stop all the
work I was doing and completely focus all my attention on this book.

The reason for this was simple: the book almost directly begins with talking about people choosing to travel to remote
locations to help them become productive. I suddenly realized this book might actually articulate the arguments I
couldn't when I was planning my wishful trip.

It still took me a couple of days, spending time inbetween chapters to think and re-reading a few chapters. But I came
out of the isolation a changed person.

## Deep Work

Let's make something clear, I am not here to sell you this book. As the other books didn't work for me, this one might
not for you. I am not here to tell you how to fix your productivity, I am simply stating a few steps that have worked for me
so far.

The book goes on to explain how important it is to work without distractions. And, as the book correctly predicted, my
reaction was:

> Well, that doesn't really apply to me.

A statement which I somewhat still stand by. When I worked, I did not have my phone with me. Depending on the work I was
doing, I would put on some music or a stream, but would turn that off if my brain needed the extra focus. So, I was
already golden, right?

Not only could I improve my handling of distractions and planning of work, the book filled in some large gaps in my
understanding of how work works. I will point out two of those wisdom potholes that the book has generously filled.

## Types of work

Not all work is created equal. Somehow, I had never stopped to consider this and use this factoid to my advantage. While
I often found myself torn between programming and responding to issues, I never considered these two activities are not
equal and therefore, I should not choose between them. There is a time for programming (deep work) and there is a time
for responding to issues (shallow work). Shallow work is not "dumb work", it just requires my head to be in a different
state than it needs to be when programming.

## Quantity of deep work hours

My biggest revelation, the one that truly convinced me of the logic put forth in the book and the one that triggered all
that followed was this (paraphrasing for brevity):

> The most productive people in the world have roughly **four hours** of deep work every day, rarely more.

This, I did not understand. I immediately started mentioning it to people around me and they said "yeah, makes sense".
I refused the notion that this made sense. How do you get stuff done with only four hours?

## My take-away

Anyway, long story short: not all work is created equal and be mindful about the deep work. Once I got these concepts
in my head, my situation improved drastically within days. 

Out with the old "as long as I am not tired, I can work a little longer" and in with the careful planning of my days around
deep work hours.

I will sit down every morning with my bullet journal, draw a timeline and start planning an ideal day around the work
that needs to happen, making sure that both the shallow works gets dedicated time, and the deep work hours are spaced
with sufficient breaks. I get annoyed when I find myself working a few minutes longer and stealing precious minutes of
mental rest.

I have even ignored my planning a few times to force work ahead of a deadline, only to notice the next day
I was feeling significantly more foggy-headed and unable to work deep. Bad me! At least, I now know what went wrong and
how I can improve it.

## Note about distractions

To deal with distractions, I took two drastic steps right after finishing the book's last page: no more fediverse, and
only use messaging apps between 9:00-10:00 and 17:00-18:00. This helped tremendously in my quest for productivity!

With regards to messaging apps: I still roughly follow this pattern. On resting days, I will allow a bit more
"connected" time (helpful with family across multiple countries).

With regards to fediverse: I have noticed that it's a part of my life that I am beginning to miss, because
(most of the time) it generated a pleasant distraction and even connection. I have had truly meaningful interactions on
the fediverse. I have even received messages from a few concerned netizens that noticed and were worried about my sudden
online disappearance, something that I have appreciated a bunch! So now, I am slowly returning to the fediverse, aware
of the negative side of distraction, and whole-heartedly embracing its benefits.

Thanks for reading, here's to hoping it may help another wandering soul.