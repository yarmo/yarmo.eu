+++
title = "Time to #DeleteKeybase"
slug = "deletekeybase"
date = "2020-05-08 11:54:54"

aliases = ["post/deletekeybase"]

[taxonomies]
tags = ["short"]
+++

`#100DaysToOffload >> 2020-05-08 >> 014/100`

If you are reading this, there's a big chance you already heard the news: Zoom acquired Keybase. Whether you liked it from the beginning or not, I think most can agree that after the acquisition, there's no more reason to trust the platform and thus to use it. What happens to our keys now is anyone's guess.

Luckily, I had the precaution to never upload my private keys, so I all had to do was donate the remainder of my stellar coins to good causes (such as [Tails](https://tails.boum.org/donate/)), press the [big red button](https://keybase.io/account/delete_me) and remove any links to them from my website.