+++
title = "A place for notes"
slug = "notes-section"
date = "2020-05-12 22:57:59"

aliases = ["post/notes-section"]

[taxonomies]
tags = ["short", "meta"]
+++

`#100DaysToOffload >> 2020-05-12 >> 017/100`

## The #100DaysToOffload challenge

Participating in the #100DaysToOffload is fun and encourages to think less and do more when it comes to blogging. That last part both sounds good and bad.

It's good because more content is actually published, it discourages one to keep a post in a "draft" status for an indeterminate amount of time and, well you know how that goes, the post never gets published. It teaches you a habit of working in a permanent cycle of thinking, writing, posting and moving on to the next cycle.

But the drawback is two-fold. Content quality can be diminished. I have noticed I'm not always content with the phrasing of certain sentences. I also regularly get reminded that a post lacks certain disclaimers or counter-arguments to the main rationale.

The other issue I'm currently facing is flooding. I see my personal website as having a professional utility as well: I'd like to point potential employers to my blog so that they can get a real sense of how I think and what I am good at. Administering a homelab, keeping DNS records, thinking about social structures on the internet, etc. I'd like for that "long-form" content not to be drowned out by waves of "short-form" posts because of a challenge.

## The solution

I considered tags and though I definitely need them, they are not the solution. The default view would still contain all the posts. Also, I'm not looking forward to making a RSS feed based on (excluding) tags.

Inspired by [Kev](https://fosstodon.org/@kev) and a discussion with [Ali Murteza Yesil](https://fosstodon.org/@murtezayesil) (thanks again :D), I've decided to implement a [notes](/notes) section meant to contain all the short-form posts. Random thoughts go in the [notes](/notes), elaborate thoughts go in the [blog](/blog). A separate RSS feed will be implemented very soon. A note could also be a link to a blog post.

## Continuing the challenge

I will continue the challenge with posts being either a blog post or a note. I will, however, refrain from posting every day. Some days are devoid of post-worthy thoughts, some days do not allow for proper writing time. I will not write notes in advance, that defeats the purpose of the challenge.

I'm already noticing benefits from participating: I take more time to write, I post more and that leads to me having more interesting discussions. I am thankful for its existence but will also adapt my participation to my lifestyle and schedule.

## Update 2022-05-03

I'm removing the notes section in favor of the [#short](/tags/short) tag. All posts short and long are available in the [/blog](/blog) section.