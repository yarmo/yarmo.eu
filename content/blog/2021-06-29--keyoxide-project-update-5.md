+++
title = "Keyoxide Project Update #5"
slug = "keyoxide-project-update-5"
date = "2021-06-29 14:52:10"

aliases = ["post/keyoxide-project-update-5"]

[taxonomies]
tags = ["keyoxide"]
+++

An update for all.

## Accessibility

The latest 3.1.0 release of [keyoxide-web](https://codeberg.org/keyoxide/keyoxide-web) greatly improves accessibility 
and ensures that it works nicely together with screen readers.

To make sure the implementation of accessibility features was as thorough as possible, it was first ran through a series
of automated tests, namely [Lighthouse](https://web.dev/measure/) and [WAVE](https://wave.webaim.org/), both giving
Keyoxide respectively a **100% accessibility score** and **0 accessibility errors** on every page.

While automated tests are a decent start, nothing beats feedback from the actual target audience: good ol' human beings.

After [posting a message](https://fosstodon.org/@keyoxide/106380848176122986) on the Keyoxide fediverse account 
([keyoxide@fosstodon.org](https://fosstodon.org/@keyoxide)) to call for help from people who use accessibility tools
like screen readers, I received plenty of feedback about little quirks that went undetected by the automated tests.
These were all addressed and fixed.

So I can now gladly confirm that the Keyoxide website should be **WAI-AA** compliant, meaning all text has a contrast
ratio higher than 4.5:1, all links and images are appropriately labeled for screen readers and even the profile pages 
can be navigated by keyboard alone.

I once again thank the people that have provided the invaluable feedback without whom the result of my efforts would
have proven unsufficient.

If you find more quirks and/or annoyances, please do file an issue on the
[code repository](https://codeberg.org/keyoxide/keyoxide-web/issues) so it can be fixed as quickly as possible.

## #keyoxide on IRC

Keyoxide was just about to request a channel on freenode when sadly, well, *that* happened.

So now, it is with delight that I can now invite you all to our **#keyoxide** channel on the great
[libera.chat](https://libera.chat/) network. In addition to our Matrix room, this is one more place where we can hang
out and discuss the future of identity on the internet. And many other things.

And yes, of course I have already proven my identity on IRC using the
[IRC guide on Keyoxide](https://keyoxide.org/guides/irc).

## Signing off

For all your questions and suggestions, be sure to join the conversation in the
[Keyoxide matrix room](https://matrix.to/#/#keyoxide:matrix.org) or the #keyoxide channel on 
[libera.chat](https://libera.chat/). Or raise an issue on [Codeberg.org](https://codeberg.org/keyoxide/).
All contributions (including PRs!) are welcome.

As always, the source code is available at the [Codeberg.org repo](https://codeberg.org/keyoxide/keyoxide-web).

All work on Keyoxide is possible thanks to donations, the project stands against VC funding. If you feel like Keyoxide
is a step in the right direction for netizens worldwide, please [become a patron](https://liberapay.com/Keyoxide/) and
help the project do its part in the global fight against the internet corporations.

Until next time,  
Yarmo