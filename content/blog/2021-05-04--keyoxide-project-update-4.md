+++
title = "Keyoxide Project Update #4"
slug = "keyoxide-project-update-4"
date = "2021-05-04 17:52:10"

aliases = ["post/keyoxide-project-update-4"]

[taxonomies]
tags = ["keyoxide"]
+++

The update I have been looking forward to for months.

## Keyoxide 3.0.0

Every day I work on Keyoxide, I learn more and gain a deeper understanding of how powerful this Decentralized
OpenPGP-based Identity verification actually can be. And as we are nearing the first anniversary of the Keyoxide project,
I realized all the new ideas and major improvements&mdash;most suggested by the community&mdash;were being held back by
the previous implementation of the code, restricted by my earlier understanding and imagination.

It was time not for a number of superficial additions and fixes, but for a big overhaul of the core code which would
then cause a chain reaction of bugs to be fixed and features to be added or improved.

To illustrate what is new in this version, here's my
[Keyoxide profile](https://keyoxide.org/9f0048ac0b23301e1f77e994909f6bd6f80f485d).

### Visuals

Ok, let's do start superficial, though. Keyoxide 3.0.0 has a shiny new look. I hope you will agree with me that that
was much needed. The previous design of the website was made before I even implemented the concept of decentralized
proofs.

The new and cleaner design has eliminated most of the clutter and puts all the emphasis on what is important: the
identity claims.

### Server side rendering

Thanks to the class-based approach of the [doip.js library (version 0.12.*)](https://codeberg.org/keyoxide/doipjs/),
Keyoxide will now do most of the mundane work on the server and let the browser finish the process of identity
verification. So who does exactly what now?

- The server will try and find the public key associated with the profile to be generated
- The server will parse the identity claims stored inside the public key
- The server will match the identity claims to the known library of service providers
- The server will render the profile page, including the yet-to-be-verified identity claims, and send it to the browser
- The browser will parse the yet-to-be-verified identity claims and verify them

Not only is the website now much faster to load, the browser will verify the identity claims in parallel! No more
waiting for that one slow identity to verify before showing the result of all the other identity verifications.

### rel="me"

The wait is finally over! Server-side rendering means that Mastodon instances can now detect the **rel="me"** links
on Keyoxide profile pages and will reward you with a green tick for every Keyoxide profile you link to in your
Mastodon bio!

Here's an example: [@keyoxide@fosstodon.org](https://fosstodon.org/@keyoxide). So satisfying!

Yes, this means Keyoxide can now do and be as much as "any other" identity provider on Mastodon. By just using basic
web technology. Without requiring special server protocols. And no VC-funded companies needed.

Small web truly is beautiful, isn't it? (quote from [Small Tech Foundation](https://small-tech.org/))

### A claim failed, what does that mean?

The issue of a claim failing to verify is actually more complex than it seems, and something that the previous versions
of Keyoxide did not handle very elegantly.

As an example, let us claim to be Alice on Github. If it fails, it could either mean that we made a mistake somewhere,
or we are attempting to impersonate Alice&mdash;the very thing Keyoxide is designed to detect and prevent.

In this case, it's simple: the claim `https://gist.github.com/Alice/...` could only reference Github so the story ends
here.

But what if we wanted to verify `https://alice.tld/apps/live`? From the looks of it, it could be an
[Owncast](https://owncast.online/) server, but that is just a guess.

When this claims fails to verify, does it fail because that Owncast server is not mine (impersonation) or because it
wasn't actually an Owncast server? This URL could also very well lead to a repo on a [Gitea](https://gitea.io/) server.

And what if it also fails to verify as a Gitea account? Was it one of them that genuinely failed, or neither of them?

Keyoxide 3.0.0 now recognizes "ambiguity" in URLs and acts accordingly. Does a claim with an unambiguous URL (like
Github) fail? Keyoxide will let the visitor know the claim genuinely failed. Did a claim with an ambiguous URL fail?
Then Keyoxide will show a message letting the visitor know that it wasn't sure what the claim was meant to be but
regardless, it failed to verify.

## Future improvements

Keyoxide 3.0.0 brings a few tweaks, but again, the biggest change is the overhaul of the core code. This will allow
a bunch more improvements to be made soon with relative ease. Here's an overview of what is in the pipeline.

### Requirement of JavaScript

Previous versions of Keyoxide said "the browser must do everything". This meant that JavaScript had to enabled in order
for Keyoxide to be able to do anything at all.

As stated above, Keyoxide 3.0.0 now only lets the browser do the very last step of the whole process but this still
means JavaScript is required. However, it is not difficult to imagine now that the server could do everything and just
send the finished profile page to the browser.

In a future version of Keyoxide, visitors who have JavaScript disabled and do not mind waiting for up to fifteen seconds
(due to some claims taking more time to verify) will be able to request a fully server-side rendered profile page.

### a11y and i18n

With more work being done server-side, it becomes simpler to implement internationalisation and render the website in
different languages.

Also, with the pages themselves become less dynamic, decent accessibility is also simpler to achieve and currently has
the highest priority.

## Signing off

That's about it for today. This update marks a big change that will greatly benefit future versions of Keyoxide. I can't
wait to start working on the next developments and share them with you as they come along.

As always, the source code is available at the [Codeberg.org repo](https://codeberg.org/keyoxide/keyoxide-web)
(now renamed to **keyoxide-web**).

For all your questions and suggestions, be sure to join the conversation in the
[Keyoxide matrix room](https://matrix.to/#/#keyoxide:matrix.org) or raise an issue on
[Codeberg.org](https://codeberg.org/keyoxide/). All contributions are welcome!

Until next time.
