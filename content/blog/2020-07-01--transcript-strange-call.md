+++
title = "Transcript of a strange call"
slug = "transcript-strange-call"
date = "2020-07-01 16:14:40"

aliases = ["post/transcript-strange-call"]

[taxonomies]
tags = ["short"]
+++

**Lady**  
"Hi, I'm calling you because you have shown interest in the financial market."

**Me**  
"What? No, I haven't."

**Lady**  
"You have in the past&hellip;"

**Me**  
"I think you might have the wrong person on the line."

**Lady**  
"Oh, what is your email address?"

**Me**  
"I'm not giving you my email address."

**Lady**  
"It's all right, I have it here, I just wanted to check. What is your full name?"

**Me**  
"No, I'm not&hellip; Wait&hellip; Why? I'm not interested in the financial market."

**Lady**  
"You were in the past&hellip;"

**Me**  
"Ok, bye."
