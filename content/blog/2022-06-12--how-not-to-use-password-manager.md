+++
title = "How not to use a password manager"
slug = "how-not-to-use-password-manager"
date = "2022-06-12 21:39:59"

[taxonomies]
tags = ["short"]
+++

I have just read Kev's latest blog post named [Segregating Email With Subdomains](https://kevq.uk/segregating-email-with-sub-domains/) and it's a trick I am most definitely adopting. I enjoy using catch-all addresses over `+something` addresses as the latter are rejected by some services. But only one person per domain can enjoy this privilege. If each member of the server gets their own subdomain, they all get to enjoy a catch-all address. Simple yet clever.

But I feel an urge to quickly react to the post's premise and its stated "problem with the setup".

Both the author and a friend of theirs have found themselves in the predicament of not being able to reset their forgotten password because they also forgot the email address they used to sign up for the service in question.

At the risk of sounding like Captain Obvious, password managers are an all-or-nothing solution. There is nothing to be gained by "not putting the credentials to this one service in the password manager".

Every time you don't use a random password issued and tracked by your password manager, you are using a password you will need to remember and so will either be one you use more often, or is some derivative of the service you are signing up for. This opens your account up to a host of attack surfaces.

I take pride in not knowing any of my passwords anymore — except for the big one for the password manager. Yes, it takes a bit of time to get used to the system but once you have it, it just makes no sense not to use for every single service you sign up for.

And in my humble opinion, laziness is the worst reason not to register a password in the password manager. This is not the reason stated in the blog posts but I am adding this as a general statement.

> it’s easy to lose track of which address you have used where

It doesn't have to be. Password managers are not password managers, they are identity managers. Therefore, we should extend the identity "randomness" also to the email addresses we use.

Up to a few weeks ago, I used a catch-all address combined with a random string generator, the result of which I stored in Keepass. Cumbersome, granted, but effective. If you don't have a catch-all address, a plus address works for those services that don't reject them.

But when [Bitwarden recently announced support for email forwarding services](https://bitwarden.com/blog/add-privacy-and-security-using-email-aliases-with-bitwarden/), I nearly immediately migrated to their service as this would streamline my process — and add a dash of anonimity as long as we trust our email forwarding service of choice.

> Yes, my password manager is supposed to remember the email used, but that’s not always reliable

With all due respect, I can't imagine a single situation where my password manager's capacity to store the credentials I entered isn't *reliable*.

So, in short, we can (and should) discuss to great lengths how to properly use a password manager — it's a complicated matter. But skipping some accounts is for sure **not** how to use a password manager.