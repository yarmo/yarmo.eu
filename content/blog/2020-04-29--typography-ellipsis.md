+++
title = "Typography &middot; Ellipsis"
slug = "typography-ellipsis"
date = "2020-04-29 21:21:43"

aliases = ["post/typography-ellipsis"]

[taxonomies]
tags = ["short", "typography"]
+++

`#100DaysToOffload >> 2020-04-29 >> 005/100`

I like typography and exploring the stories behind special characters. Today, I'd like to talk about one that many use frequently, myself included, but often not in the "digitally correct" way (IMHO).

Yes, I'm talking about the ellipsis. Symbolised by three consecutive dots, it signals that a sentence was cut short and the reader can finish it in his or her head by knowing the context. Surrounded by brackets, it is used to signal a passage was ommitted but the meaning of the remaining sentence is unaltered by that omission. Messaging apps use it to signal the other person is writing.

You may or may not know this, but both on our computers and on our phones, the ellipsis is actually a special characters which can be used instead of writing three separate dots. On a phone, it's accessible under one of the keys by long-pressing on it. On the computer, I usually just copy-paste it, but on Ubuntu, it's inserted by pressing `ctrl+shift+u`, then typing `2026` followed by an `enter`. On Windows, it's inserted by pressing `alt + 0 1 3 3` on the numpad. In both HTML and markdown, it's inserted by writing `&hellip;`.

[Wikipedia](https://en.wikipedia.org/wiki/Ellipsis)