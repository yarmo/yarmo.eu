+++
title = "Selfhost email… But should you?"
slug = "selfhost-email-drawbacks"
date = "2020-05-03 19:46:47"

aliases = ["post/selfhost-email-drawbacks"]

[taxonomies]
tags = ["selfhosting"]
+++

`#100DaysToOffload >> 2020-05-03 >> 009/100`

Yesterday, I wrote about [how you **can** selfhost your very own email server](/blog/selfhost-email). Shortly after publishing the post, it was pointed out to me that are very reasonable drawbacks to doing this. So today, let me give you my answer to the question of whether you **should** selfhost your email.

## Relying on hardware

Firstly, I mentioned in that article that although I have two domains on my selfhosted server, I still fall back to a protonmail address for the most important stuff like banking and governmental services. So what's the point of selfhosting then, if I do use third-party email addresses? Well, what I failed to mention was that in the long term, yes, I do want everything selfhosted.

When I started my email hosting adventure, I was very cautious. Only months before did I start my own homelab and as it turned out, that had the tendency to crash every so often, making it a no-go for email hosting. I resorted to use a VPS while getting my homelab sorted out. This worked great and still does, but at the time, you can imagine I was still discovering the DNS parameters, the reputation handling (more on this later). Also, what were the consequences of running a VPS for 24/7? I could not commit to using the selfhosted email for anything more than experimentation.

Fastforward about a year and the VPS has held up greatly, the email software has never crashed or acted against my expectations. It has received regular updates and never failed once during one. Meanwhile, my homelab has proven to be extremely reliable and with the upcoming hardware upgrade, I expect even less irregularities than I do now. Soon enough, when all the stars align and I figure out how to make recoveries as fast on my local hardware as I can on a VPS (make a new instance based on a daily snapshot and voilà!), my email server will be transferred to my homelab and I will use it for everything.

## The pain of administering an email server

Secondly, I've also heard of people not resorting to selfhost their email because of the fragility of the underlying processes and if one thing is slightly out of tune, the whole email server stops working. Although I've toyed around with most of the individual processes like dovecot at the beginning to understand what they do and how they work, I haven't touched a single one of them in almost a year. [mailcow.email](https://mailcow.email/) is just that good. I've played around with the settings and it won't stop working. Meanwhile, I get an antivirus, spam monitoring, those handy "+topic" email filtering. I'd like to try out [Mail-in-a-Box](https://mailinabox.email/), mostly because it is also recommended by [PrivacyTools](https://www.privacytools.io/providers/email/#selfhosting) but I have no incentive too. My current solution just works great for me.

## The reputation of a server

Lastly, I need to address a IMO bigger problem: reputation. If other servers don't trust you, your emails may easily be thrown into the spam folder of the recipient or even rejected. The main reason for this is to fight spam: mass email spammers usually operate from unknown IP addresses. Unfortunately, this hurts the selfhosters. So, before you have even installed your email server software, you are already mistrusted by simply not using the big servers like Gmail and Hotmail. And indeed, when I started, most of my emails landed in spam.

This got greatly improved simply by using an email relay; in my case, [mailgun](https://www.mailgun.com/). These paid-for (but often with free tier) services are a lot more trusted since mailgun will do spam prevention on their end, so letting them send your emails for you is a great improvement. And even with the [recently reduced free tier](https://news.ycombinator.com/item?id=22192543), I don't send nearly enough emails to come close to the free tier quota.

However, it still happens that my emails are treated as spam so I often do follow ups via other channels of communication. Another issue may be that the IP address you were given already has a bad reputation caused by a previous owner: this is difficult to find out and even harder to fix. DDG-ing `improve email server reputation` yields many articles but read a handful of them and soon you'll realise it's really really hard to improve it. There's no central repository, no forms. Getting a mistrusted IP address can quickly suck all the fun out of having your own email server.

## Answering the question

So, **should you**? This depends on how willing you are to be independent of third-party email services and how much you are willing to put up with. I started naïvely and had to answer this question along the way while experimenting. By now, my personal answer to this question is: yes. I see the benefits and drawbacks. I'm not sure if it's the usage of mailgun, or me sending mails to family and friends and then asking them to tell their services to not mark it as spam, but most of my emails are properly received nowadays. Also, I have managed to improve my infrastructure, I can rely on the hardware (and soon on the emergency recovery mechanisms) and will soon migrate my email server so it's nicely at home.

Hosting your own email server is not easy and requires your full dedication. And with many upcoming [trusted and privacy friendly email services](https://www.privacytools.io/providers/email/), it may not always be the right tool for the job.