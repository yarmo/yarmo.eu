+++
title = "#100DaysToOffload"
slug = "100-days-to-offload"
date = "2020-04-25 11:14:33"

aliases = ["post/100-days-to-offload"]

[taxonomies]
tags = ["short"]
+++

`#100DaysToOffload >> 2020-04-25 >> 001/100`

On [Fosstodon](https://fosstodon.org), [@kev](https://fosstodon.org/@kev) wrote a [toot](https://fosstodon.org/web/statuses/104053977554016690) which started [#100DaysToOffload](https://fosstodon.org/tags/100DaysToOffload), a challenge to blog for 100 days about anything. Enthusiastic about this idea, I'm starting today and decided to make a continuously updated list about the other blogs participating in the challenge.

[Beyond the Garden Walls](https://write.privacytools.io/darylsun/)  
[G's Blog](https://blog.marcg.pizza/marcg/)  
[Freddy's Blog](https://write.privacytools.io/freddy/)  
[Roscoe's Notebook](https://write.as/write-as-roscoes-notebook/)  
[Nathan's Musings on the Web](https://degruchy.org/)  
[Gregory Hammond](https://gregoryhammond.ca/blog/)  
[Garron](https://www.garron.me/en/blog/)  
[Secluded Site](https://secluded.site/)  

Want to find even more participating blogs and links to every post? Search for the `#100DaysToOffload` hashtag on the fediverse ([Fosstodon link](https://fosstodon.org/tags/100DaysToOffload)).
