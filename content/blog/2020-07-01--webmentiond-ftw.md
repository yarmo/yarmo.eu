+++
title = "Webmentiond FTW"
slug = "webmentiond-ftw"
date = "2020-07-01 10:31:49"

aliases = ["post/webmentiond-ftw"]

[taxonomies]
tags = ["short", "nuggets"]
+++

I just read [this post by Guillermo](https://www.garron.blog/posts/webmentiond-working.html) which is a great general overview of [webmentions](https://indieweb.org/Webmention) and in particular, the implemention by Horst Gutmann named [webmentiond](https://zerokspot.com/weblog/2020/06/14/setting-up-webmentiond/). An absolute delight to use! Glad you got it working, Guillermo, and happy to have been of help!
