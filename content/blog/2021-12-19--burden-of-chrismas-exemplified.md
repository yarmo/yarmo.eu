+++
title = "The Burden of Christmas, exemplified"
slug = "burden-of-christmas-exemplified"
date = "2021-12-19 13:00:16"

[taxonomies]
tags = ["christmas"]
+++

## Introducing CB

Something happened yesterday which subsequently consumed a significant portion of my attention for the rest of the day and I decided to try and put into words what happened, what I thought, how I reacted and how I should have reacted.

While this post is the consequence of an interaction with a friend I'll nickname Carte Bleue (she'll understand =D) or CB for short, anything I say here is in no way a critique on her. This rant is directed towards Christmas and its commercialization. This interaction could have happened with anyone. It is my privilege to be able to consider CB a friend in my life and there is nothing I would to jeopardize that.

## The Jar of Cookies

For no particular reason, I baked cookies yesterday. Dutch kruidnoten ([wikipedia](https://en.wikipedia.org/wiki/Kruidnoten)) to be precise. I bought most of the spices I needed back in October and missed the date to bake the cookies on the 5th of December as I had planned (due to lacking a mortar and pestle and a few more spices).

So yesterday, unsure what to do with my day, I went out, treated myself to a mortar and pestle, purchased a few more spices and baking soda and got baking.

While the dough was in the fridge doing whatever it is dough does in a fridge, I remembered CB was dropping by later in the afternoon for a minute or less to drop something off. The proverbial lightbulb ignited.

I found an old glass jar, cleaned it, filled it with fresh-out-of-the-oven kruidnoten, wrote "Joyeux Noël" with a smiley on a small piece of laid paper, attached the note to the jar using some cotton string and called it a day. It looked cute AF :)

Time passed. The buzzer rang. I opened the door. There was CB. I handed over the jar of cookies.

After an expression of gratitude, she said…

"Well, now I have nothing for you."

Queue the rant.

## Hypocrisy much

Again, this is no way a critique on CB. Anyone I know would have reacted the same way. Hell, I would have reacted that way! So yeah, everything I am about to say applies to myself as much as it does to anybody else.

## Newton's Third Law of Christmas Gifting

> For any Christmas gift gifted, an opposite gift is expected.  
> — Isaac Newton (somewhat paraphrased)

This law that governs modern-age Christmas rituals is what I and many others call the Burden of Christmas.

How is it possible that I cannot give a jar of cookies without giving a feeling of guilt in one and the same gesture? I did not expect a gift in return. In fact, [I'd rather not receive gifts at all](https://yarmo.eu/blog/2020s-christmas-gift-pledge/).

## Human nature?

You might counter with the following argument: it's just human nature. We don't like debts. Living in a society means giving and taking. A good-natured person wouldn't want to take without giving back.

Yeah, I get that, but hear me out. Had I handed over the jar in August or any other month that does not start with "D" and end with "ecember", none of this would have happened. CB gave me a jar of a homemade product a year back, I did not feel an urge to immediately return the gesture.

What is it about Christmas that completely changes our perspective and our primary response to receiving a gift?

I don't know for sure. If you have interesting reading material on the subject, please [do share with me](https://yarmo.eu/contact/), I'm genuinely curious.

## Guilt flows both ways

So I intended to give a jar of cookies and instead I handed over a burden.

Could I not have foreseen that?

Well, as a matter of fact, I did. There was a moment in the process yesterday when I strongly considered not giving the jar and wait until January. Because there was a chance she would express guilt for not having something to return on the spot. Because if that would happen, I would feel guilt for putting her in that position. And I wanted none of that. I just had a surplus of cookies.

Damn you, Jar of Cookies, source of all evil!

Seriously though, this is messed up.

I'm certain many people don't have these issues. They know each other well enough to look through this reflective and hazardous haze and see the things for what they truly are.

But I suspect that many among us do have to contend with the Burden of Christmas during the December month. Or November. Or whenever it is the Christmas shitshow starts these days.

Or maybe not and I am just weird. Very plausible too.

## Game theory applied to Christmas

This whole story strongly reminds me of the Prisoner's Dilemma ([wikipedia](https://en.wikipedia.org/wiki/Prisoner%27s_dilemma)) and, according to the dozens of online articles I just found ([DuckDuckGo search results](https://duckduckgo.com/?q=christmas+prisoner+dilemma)), it does so for many other people too.

Since we don't usually communicate our intention to prepare gifts for others, how are they supposed to know if they should prepare something in return? After all, you don't want to be that person that doesn't gift back, or so society tells us. So it's in your best interest to prepare a gift for the largest number of people, just in case. Which leads to all those people facing the same dilemma. And so on.

Huh, I wonder if the marketing industry has caught on to this phenomenon and would use it to their advantage. [/S](https://www.urbandictionary.com/define.php?term=%2FS)

## My reaction

So how did I react?

Since putting her on the spot put me on the spot, I don't remember with 100% certainty, but I think I chuckled and said "That's okay!".

O agony, thee malignant incubus!

"*That's okay*"???

This implies a gift in return would indeed have been a desirable outcome, but "hey, it's okay, I forgive you". That was not my intended message!

So in the blog version of the "esprit de l'escalier" ([wikipedia](https://en.wikipedia.org/wiki/L%27esprit_de_l%27escalier)), what should I have said?

I am not sure. Anything along the lines of "well, that was not the intention" may come across as disdainful, as in "I wasn't expecting you to give something in return".

I am most certainly overthinking at this point. Any response would probably be acceptable as long as you point out that it was not your intention to give them the Burden of Christmas.

Perhaps this blog post is also a half decent—if delayed—response. If I could have condensed the entirety of this post in a 30 seconds monologue, that might have done the trick.

## To CB

I am not sure yet if I'll send a link to this to CB. I am quite conscious of my tendency to overthink and assuming (or hoping) that others don't.

But if I did, well, hey there CB :)

Hope you enjoyed the cookies! They were a silly little thing I just wanted to give you. For fun. Made me feel happy to prepare the jar. I tried painting a message on it but that miserably failed :)

I don't want anything in return, even though it's December. If you know me well enough, you'll understand.

And if that still doesn't alleviate the Burden of Christmas, let's organize a fun movie night again. As of January. After all the madness has died down.