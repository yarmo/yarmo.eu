+++
title = "Why we won't have artificial intelligence rivaling human intelligence"
slug = "ai-vs-human"
date = "2020-05-17 00:27:27"

aliases = ["post/ai-vs-human"]

[taxonomies]
tags = ["ai"]
+++

`#100DaysToOffload >> 2020-05-17 >> 019/100`

Someone asked why people are working on artificial intelligence "which would infinitely surpass human capabilities?" Here's my answer.

That is not going to happen. We will never be able to produce machine learning surpassing human intelligence (though I would have personally looked forward to it).

Consider this: neurons have refractory periods of 1-4 ms. During the refractory period, a neuron cannot give another signal. Thus, their firing rate cannot exceed at absolute best 1000 signals or "spikes" per second. That's 1 kHz. At best. Your average neuron is a lot slower. Modern day processors easily exceed that speed by a factor 1000. So why don't we have a cyborg Einstein yet?

That has everything to do with makes us "intelligent". We have the same neurons as primates. Heck, we have the same neurons as worms. Why aren't people afraid the worms might kill us all soon?

Intelligence does not stem from the number of neurons or how fast they are. It all has to do with how they are connected.

Humans have extremely well-developed cortices. The reason cortices have folds is to increase the surface area, just like a radiator has folds. Sure, you gain a few neurons, but more importantly, you gain a whole lot of connections.

So, "researchers just need to make CPUs with more connections to increase their capability", you might say.

Well, you still haven't considered the single most important reason artificial intelligence will always be inferior to any animal intelligence.

CPU's are made of transistors. A transistor is a switch that goes on or off.

Brains are made of neurons. A neuron has an immense range of output, from slow firing to fast firing (up to 1 kHz). That is a whole lot more nuanced that on/off. It has also extremely well-calibrated inputs, it can receive multiple excitatory inputs that increase neuronal activity, it can also receive inhibitory inputs that decrease activity. It can do addition and substraction by placing the inputs at different locations on the dendrites (what neurons use to capture inputs). Each neuron can singlehandedly do what a whole CPU is designed to do.

And there we have it. To equal a brain with millions of neurons, you can't use a CPU with millions of transistors. You'd need a computer with millions of CPUs.

You, my friend, are safe.