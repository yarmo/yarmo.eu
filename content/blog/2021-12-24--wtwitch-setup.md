+++
title = "My wtwitch setup"
slug = "wtwitch-setup"
date = "2021-12-24 23:31:17"

[taxonomies]
tags = ["short", "streaming", "cli"]
+++

## wtwitch

wtwitch ([source repo](https://github.com/krathalan/wtwitch)) is a neat terminal user interface for Twitch, allowing me to watch Twitch streamers without needing to ever use their website or any of their clients.

It manages subscriptions, has autocomplete and easily let's you start a stream in an mpv window or any player of your choosing.

I have a basic setup with the config file in the default place.

`~/.config/wtwitch/config.json`:

```json
{
  "player": "mpv",
  "quality": "best",
  "colors": "false",
  "printOfflineSubscriptions": "true",
  "subscriptions": [
    {
      "streamer": "rossmanngroup"
    },
    // and many more
  ],
  "apiToken": "...",
  "apiTokenExpiry": "...",
  "lastSubscriptionUpdate": "..."
}
```

## wtwitch and rofi

Where things get interesting is the integration with rofi ([source repo](https://github.com/davatorium/rofi)), the application launcher and so much more.

All I need is a single executable file.

`~/.local/bin/rofi_wtwitch.sh`:

```bash
#!/bin/bash

if [[ $1 ]]; then
	name=$(echo $1 | awk {'print $1'} | sed 's/\://')
	wtwitch w $name > /dev/null
else
	wtwitch check | sed -n '/Live/,/Offline/p' | sed '/Live channels/d;/Offline/d' | sed 's/\x1B\[[0-9;]\{1,\}[A-Za-z]//g;s/   //;'
fi
```

### Running straight from terminal

```
rofi -modi wtwitch:/home/yarmo/.local/bin/rofi_wtwitch.sh -show wtwitch
```

### For bspwm/sxhkd users

`~/.config/sxhkd/sxhkdrc`:

```
# Launch wtwitch in rofi
super + shift + d
	rofi -modi wtwitch:/home/yarmo/.local/bin/rofi_wtwitch.sh -show wtwitch
```

Replace with your own keybinding.

### For i3 users

`~/.config/i3/config`:

```
# Launch wtwitch in rofi
bindsym $mod+Shift+d exec --no-startup-id "rofi -modi wtwitch:/home/yarmo/.local/bin/rofi_wtwitch.sh -show wtwitch"
```

Replace with your own keybinding.