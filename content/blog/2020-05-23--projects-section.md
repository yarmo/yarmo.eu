+++
title = "A new Projects section"
slug = "projects-section"
date = "2020-05-23 22:51:43"

aliases = ["post/projects-section"]

[taxonomies]
tags = ["short", "meta"]
+++

`#100DaysToOffload >> 2020-05-23 >> 024/100`

I've added a new [Projects](/projects) section to my personal website, the new home for projects I'm either still thinking of doing or actually developing, As these projects will be open-source, so will my preparation for them.

The benefit of doing this is that when you look around and see a project you like or have experience with, I would love for you to [contact me](/contact) to work together.

As of today, there are only two projects listed, I have more in my head which I will write down over the coming days.