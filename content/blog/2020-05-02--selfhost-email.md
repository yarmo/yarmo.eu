+++
title = "Selfhost email"
slug = "selfhost-email"
date = "2020-05-02 16:20:35"

aliases = ["post/selfhost-email"]

[taxonomies]
tags = ["selfhosting"]
+++

`#100DaysToOffload >> 2020-05-02 >> 008/100`

Yes, you can selfhost email. And you should, if and ONLY if you feel comfortable with maintaining a linux server. I'm not a pro at all, but I've been doing it for almost two years, I know where to find my logs, I know how to find the correct answers on stackoverflow and troubleshoot a less-than-functional system.

So don't start with this, but eventually, soon enough, you can selfhost your email.

Because email is important to me, I have chosen to not host it at home, any network issue could prevent emails from coming in. Granted, the sending server will usually retry for 24 hours until the email is actually received on your side so small errors are forgiven, but still, I've opted for a dedicated droplet on digitalocean, though any VPS will do.

And then, follow the instructions on [mailcow.email](https://mailcow.email/) and you're set. SSH in once a week on your VPS to run the updater. The administration side has plenty of features for advanced administration of the email server and the included webclient is the awesome SOGo.

If you want to make sure your email server is as trusted by other servers as possible, your emails are sent as securely as possible and your experience with other email clients is as smooth as possible, please check out my [post on email server DNS settings](https://yarmo.eu/blog/email-dns).

With all that being said, I still use a protonmail address for critical websites and services like governmental services and banking, because whatever happens, I need to make sure that I really receive these emails. On my selfhosted email server, I use two domains: one which I share with the world and with websites for logins and one that I keep private and only use for direct communication with other people. I have yet to experience a single minute of outage, credits to digitalocean and the people behind mailcow.

---UPDATE---

After a [fair comment](https://fosstodon.org/@Matter/104099349377193869) on the fediverse, I have written a [follow-up post](/blog/selfhost-email-drawbacks) to address a few more critical points like server reputation and how I deal with that.