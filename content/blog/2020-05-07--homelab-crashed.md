+++
title = "My homelab crashed, time for a break?"
slug = "homelab-crashed"
date = "2020-05-07 19:53:23"

aliases = ["post/homelab-crashed"]

[taxonomies]
tags = ["short", "homelab"]
+++

`#100DaysToOffload >> 2020-05-07 >> 013/100`

It's not the first time my homelab has crashed and it won't be the last time. Something with the hard drives. I'll figure it out, no doubt. But despite needing it for various services throughout my daily routine, I have decided to let the homelab rest for a few days maybe.

It has been running almost non-stop since I started it about two years ago, I never made major changes, always gradually improved upon it. Now, the time may have come to take a hard look at what I started with, what I ended up with, learn a few valuable lessons and perhaps start over. My homelab could use a 2.0 moment.