+++
title = "GPG import public key from smartcard"
slug = "gpg-import-from-smartcard"
date = "2022-05-03 09:00:12"

[taxonomies]
tags = ["short", "nuggets", "openpgp", "gpg", "smartcard"]
+++

## TLDR

On a new computer, insert your USB OpenPGP smartcard and run:

```bash
gpg --card-edit
fetch
quit
```

## Explanation

I have a [YubiKey 5](https://www.yubico.com/products/yubikey-5-overview/) (still waiting on my [Solo v2](https://www.indiegogo.com/projects/solo-v2-safety-net-against-phishing#/)) on which I store my OpenPGP secret key.

However, if I boot into a new system, insert my USB OpenPGP smartcard, import my public key from a keyserver:

```bash
gpg --keyserver hkps://keys.openpgp.org --recv-keys ABCD1234
```

[configure git](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work):

```bash
git config --global user.signingkey ABCD1234
```

and attempt to sign a commit, I'll get an error message:

```bash
git commit -S -m "Signed commit"
# error: gpg failed to sign the data
# fatal: failed to write commit object
```

GPG doesn't know yet it can interact with the private key stored on the USB OpenPGP smartcard!

So, instead of importing the public key from a keyserver, fetch it from the smartcard with the following commands:

```bash
gpg --card-edit
fetch
quit
```