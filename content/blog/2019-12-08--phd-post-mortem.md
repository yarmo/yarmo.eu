+++
title = "A PhD Post-Mortem"
slug = "phd-post-mortem"
date = "2019-12-08 21:02:14"

aliases = ["post/phd-post-mortem"]

[taxonomies]
tags = ["personal-development","phd","science"]
+++

This is one of those stories that starts with an ending.

As of January 1st 2020, a new challenge awaits for me, a new life. Because my university/academic journey will be completed. In 2010, I set out on a path that would lead me from a biology bachelor degree to a neuroscience master degree and would culminate in a four years PhD program and result in a thesis and doctorate degree to end the journey with a bang. From there, the world would have been my oyster.

That journey was traveled as planned.

Except.

After investing a little over four years in my PhD project, I must end this leg of the trip without achieving its ultimate goal, obtaining the doctorate degree, leaving the past nine years open-ended, unrewarded, uncelebrated.

<!--more-->

This post is not vindictive in nature. In the two weeks since making my decision to end my PhD project, mere weeks before the end of the contract, I have had plenty of time to come to terms with the circumstances, to accept what has happened. At the end of the day, I burdened myself with the responsibility of taking on a PhD project, therefore the eventual outcome of said project, however positive or negative, is the product of my actions and my actions alone. Any setback can be met with positive attitude and forward thinking.

This post is also not a cry for attention. That is not who I am. As a matter of fact, the old pre-PhD me would not have written this post. As hard as I'm trying, I find it difficult to figure what pre-PhD me would have done in this situation nor will I be able to, as I've changed. I have changed in ways I could not have predicted four years ago.

This post is ultimately about opening the discussion on one of the big topics people within higher education do not like to talk about: mental health.

I want to talk about mental health. Though not as severe as with other people I've talked to, I now have first-hand experience in dealing with a sinking ship and have felt the psychological toll that it takes. I can no longer look at fellow graduate students without wondering: are they suffering?

This is my call to action.

## The first years

The project started off pretty slowly, a lot of administrative tasks impeded experimentation. I spent time familiarizing myself with the environment, learned the workings of the existing codebase and other similar tasks. The first major setback was a slow cooker: the protocol describing the activities I was supposed to do needed to be approved by internal committees. This. Took. So. Long. If I had known then that the approval would come after over two and a half full years...

Waiting for approval of my protocol, I spent time learning the experiments, repeating it over and over so that when it would matter, I could eliminate myself as a factor of uncertainty. This process was far less innocent than it seemed. While my protocol was set in a scientific frame and had set goals, the experiments I was performing at first had no higher purpose other than serve as practice. As I grew more comfortable with the experiment and the protocol approval continued to be delayed, I, along with my supervisors, started looking at small results and enjoying minor victories. Sure, I wasn't allowed to yet do what I desperately wanted to, a number of experiments showed an unexpected and promising effect and I spent a few more experiments trying to understand it.

At the time, this felt exciting. I discovered stuff!

That's not what happened. These findings were not sought, they were stumbled upon. And after a couple more experiments, all failing to further our understanding of the observations, the ideas were dropped as fast as a new effect was found and more time was sunk into trying to figure that one out.

And after that observation lead to nothing, there was another one.

And another one.

I failed to see what was happening: I was chasing shiny things. The reason there is a protocol is to keep you focused. It provides a framework in which you work. It asks a question that will be satisfied by any answer, as long as this answer is obtained using the methodology prescribed by the protocol.

The solution would have been simple: write a different protocol with a simpler question. Set the framework. Gather the evidence.

I did not have this wisdom.

## The latter years

I remember the day I received the email approving the protocol. I am not able to describe the feeling of futility that overcame me. There I was, now allowed to do the things I came here to do, but knowing full well there was no longer time to set up the experiments. My project was set to answer a forty year old question. Instead, I was chasing new shiny things, grasping for any finding that could provide meaning to my presence.

For a while, the project seemed on track, I spent almost a year and a half investigating an interesting effect I observed, a shiny new thing more promising than all that came before. Yet, a shiny new thing nonetheless. Luck struck again: the interesting effect turned out to be an experimental artifact, and though I have been able to 100% confirm this, those nine months worth of data were thrown in the bin.

As a little disclaimer: we had been suspicious it could have been an artifact but there was, and still is, no proper way to test this.

Anyhow, in the third year, amidst all of this chaos, the effects of stress and impending doom were starting to take their toll. Fun routines became less fun. Joyful events inspired less joy. I became more isolated, first in the working environment, later in my personal life.

It was during one of the more difficult periods of my PhD project that happened the most happy event: meeting my girlfriend. I felt particularly down in the days leading up to when we bumped into each other, and meeting her had an immediate positive effect on me. It gave me a reason to wake up and do something, it gave me the spirit to keep the fight going, finish the project and claim the reward.

It was only a matter of time before stress caught up again. Experiments failed, stuff got delayed. The question shifted from "what is needed to finish this project" to "is there even a way to finish it at all". The last year was a race against the clock and against the requirements to hand in a thesis. Published articles were needed. More experiments were needed. Did we just throw away nine months of data? Cool, let's replace it with something new. Frustrations between my supervisors and me grew and created more stress. Weekends were spent sitting on the couch and adjusted to avoid all mental activity. Hobbies vanished. I no longer was the same cheerful person to be around anymore, and though I will always admire the strength of my then-girlfriend to put up  with what she put up with, that relationship unfortunately also did not outlast the strain the project put on me.

A little over a month before the end of my contract, after four years and two months of work, I had to call it. The project was dead. There were still escape routes to make something out of the project but I had to decline. My body declined. My brain declined.

This was about two weeks ago, and I am only just writing this now as I've spent almost the entire time incapable of thinking. I had to accept what was happening, I had to make peace with the fact that I spent my entire being on this project, it had cost me happiness, laughter and a relationship, and all in all, it was just to quit right before the end.

## Science and me

The scientific world is not for me. Perhaps a different project could have stimulated me in a better way and aspired me to become the scientist I always dreamed of becoming. But somewhere in the last four years, I realized this was not the dream for me. Knowing that obtaining a PhD was no longer vital to my career, I persisted in my efforts to finish the job as it was my way of concluding in a satisfactory manner my nine years of studying biology, neuroscience and how to become a scientist.

## Mental health and me

I wanted to finish the PhD just for me, just for my own satisfaction but in the end, I too was the reason I could not. I was becoming more susceptible to the seasonal illnesses every year, the first few days of holidays were spent working through a persistent headache. I no longer spent time on my hobbies and less and less time behind my piano. Programming, my true passion, had become a chore. And perhaps worst of all, the love for science was gone.

## Final words

So, there it is. Ending in a few more weeks, I am now working to leave the project in a suitable state as well as writing to publish one scientific article. I tried to avoid thinking too much about the future as it would only distract me from my already attention-demanding present, but the time for planning has now come. Leaving a whole lot behind, I have so much to gain.

But I will not simply forget about it all. I can not. The environment I worked in during the last few years did not inspire me to come forward about what was happening in my head. Much like a real-life Instagram, the scientific community celebrates success and disguises, or even denies, failure. Words embellish mistakes and misfortune. Posters filled with colorful graphs hide the hardships and perils experienced by an entire generation of upcoming scientists trying to make it in a world that does not welcome them. It inspires fraudulent behavior. The stories I've heard, from PhDs to PIs, from students to technicians.

Come forward. Talk, and you will find people to listen. I know there is an entire population out there of people suffering through their academic career. You are not alone. Let's discuss this. I would like to talk to you.

\#mentalhealth

The fediverse is a social network promoting free speech and provides a safe environment to find people in similar situations and have meaningful conversations. You'll find me there, [@yarmo@fosstodon.org](https://fosstodon.org/@yarmo). Let's talk.