+++
title = "Taking a break from raid"
slug = "break-from-raid"
date = "2020-05-04 18:43:31"

aliases = ["post/break-from-raid"]

[taxonomies]
tags = ["short"]
+++

`#100DaysToOffload >> 2020-05-04 >> 010/100`

I have three main hard drives in a [snapraid](http://www.snapraid.it/) setup in my NAS and a few extra drives for backup. All drives are connected to the server (NUC) via a JBOD USB drive case. I love snapraid, it has served me well and most certainly will in the future.

But now, I need the drive space more than I need a solution for my data to continue to being served while a drive has died. As we all know, raid is not a backup, it's a solution to ensure the data is available while one or more drives are not. Perfect for critical applications, but let's be honest, my homelab is not, especially with me sitting 24/7 next to it.

Thus soon, when I have saved a bit more, I will expand my homelab to a larger array of drives, all connected directly through SATA and all raided using snapraid with ample backup capacity. That time is unfortunately not now. So out goes snapraid and in goes the full capacity of my third drive.

They are WD Red 6TBs. Yes, I have checked, they are CMR. And yes, these are the last drives I'll ever buy from WD.