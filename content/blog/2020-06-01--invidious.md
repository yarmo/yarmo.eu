+++
title = "Invidious"
slug = "invidious"
date = "2020-06-01 13:05:58"

aliases = ["post/invidious"]

[taxonomies]
tags = ["short"]
+++

Small acts of resistance are all we need. Together, we make change.

## Compliance: YouTube

Everyone knows YouTube. It contains more than enough content to keep you entertained for a couple of lifetimes.

The thing is, it's owned by Google and has enough privacy-invading trackers and ads to follow and pester you during all of these lifetimes.

## Resistance: Invidious

Please consider using Invidious ([github repo](https://github.com/omarroth/invidious)), a free and open source service that sits between you the user and the YouTube servers. It eliminates ads, does not use YouTube APIs and has many features YouTube should also always have had (audio-only mode? Yes please).

Several [instances](https://github.com/omarroth/invidious/wiki/Invidious-Instances) are hosted around the world, make sure to visit the nearest to you for the best experience.

## Going beyond

But you can go further. When using Firefox, install the [Invidition](https://codeberg.org/Booteille/Invidition/issues) addon to automagically redirect YouTube links to Invidious (again, make sure to select the closest instance). On Android, install [UntrackMe](https://www.f-droid.org/en/packages/app.fedilab.nitterizeme/) to do the exact same thing, YouTube links will be opened in Invidious-compatible apps such as [NewPipe](https://f-droid.org/en/packages/org.schabi.newpipe/).

## Drawbacks

The main issue is that you are no longer supporting the content creators, which is a big issue. It's easy to say "they shouldn't be relying on YouTube and ad revenue" and I agree with that statement to some degree, but you'll still be sad when your favorite content creator quits.

Try and make contact with them, if they're small this might be feasible, if they're big then you probably don't have to worry about them quitting anyway. Ask them and push them towards accepting other methods of donation.

And then donate.
