+++
title = "The People's Web"
slug = "peoples-web"
date = "2020-04-30 08:53:37"

aliases = ["post/peoples-web"]

[taxonomies]
tags = ["selfhosting", "gafam", "tracking"]
+++

`#100DaysToOffload >> 2020-04-30 >> 006/100`

The day has long past that we should have started worrying about the openness of our web. It was only a matter of time before censorship and individual tracking would seep into the web of the western world. It pained me to see the levels of state interference in foreign countries, but at least, to me, that was something I only read about in the news, it was then unimaginable that this would happen in the short term in the "free world".

Now, in these trying times, states and big corporations may see it more than justified to track individuals and adjust the information we receive. I do not question their motives: "we are in this together". It takes the world to fight this pandemic, and the next one, and the one after that.

But the question on a lot of minds is: "how do we come back from this?" The hardest part is for the people to accept they are being tracked and their information filtered, so big corporations did it with shady and hidden tactics to avoid this confrontation. We are past this: YouTube has announced it will ban all content that does not conform to WHO and countries everywhere are building apps to track our health and social interactions. Again, this could prove to be what humanity needs right now, but what about after? Is there even an after?

Fortunately, we all have the power to make a few changes to improve our online well-being: change social networks, don't rely on corporations, self-host as much as you can. I will be dedicating a large number of posts on this topic. Self-hosting is not hard, it just takes a little effort to get started.

As a friendly reminder, this website (blog included) has no tracking whatsoever: I do not care who you are, where you are from or with how many you are reading this post.