+++
title = "Mailvelope: PGP for all"
slug = "mailvelope"
date = "2020-05-18 16:29:21"

aliases = ["post/mailvelope"]

[taxonomies]
tags = ["cryptography"]
+++

`#100DaysToOffload >> 2020-05-18 >> 020/100`

[PGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy) is a "pretty good" way of encrypting messages and files but gets often criticised for being too cumbersome to work with, which is sadly true. To counter this, certain products and services use PGP internally and provide an easy-to-use interface. Take Protonmail who uses [PGP to automatically encrypt emails between protonmail addresses](https://protonmail.com/support/knowledge-base/how-to-use-pgp/).

Handy, but we are forgetting something. If the PGP protocol is the lock, then the PGP keys are, well, the keys. Protonmail has both the lock and the key on their servers. That's not secure…

Luckily, there are more tools, like [Mailvelope](https://www.mailvelope.com/en/) ([source code](https://github.com/mailvelope/mailvelope)). It's nothing more than a browser add-on, meaning it will automatically work with any webmail service out there. Encrypting your emails becomes very simple (I also have a [more detailed guide](https://yarmo.eu/contact#mailvelope)).

- Load the recipient's public key in Mailvelope
- Open your webmail service
- Click the pink Mailvelope logo
- Choose the key of the recipient
- Write the email
- Click encrypt and send the email

That is actually quite easy and feasible for the less tech-savvy people.

But keep in mind (the usual email/PGP disclaimer): email is inherently insecure. Email metadata (including title!) is not encrypted, only the body is. Information about your secret communication can be infered from the metadata. Though PGP-encrypted emails are nice to have, truly private communication is achieved using [encrypted instant messengers](https://www.privacytools.io/software/real-time-communication/).