+++
title = "So you want to make it on the fediverse?"
slug = "make-it-on-fediverse"
date = "2020-03-20 13:54:22"

aliases = ["post/make-it-on-fediverse"]

[taxonomies]
tags = ["fediverse"]
+++

That's the plan, right? A whole new world awaits you on the fediverse, and you are going to make it there! There's something you should know.

<!--more-->

## Welcome to the Fediverse

Whenever I talk to people in my surroundings about the fediverse and try to convince them to use it, I have a go-to story that I tell. On multiple occasions in the past, there have been real-life events that sparked controversy on Twitter which then responded by banning some public figure or doing something else to upset a portion of the population. In turn, this would lead to either that public figure or a social movement to incite people to leave Twitter and join a safer and more open alternative: the fediverse. As citizens of the fediverse, we would notice a massive influx of new users and a stream of introductory messages on our timelines. Which, in my humble opinion, is always a welcome sight. An interesting observation is that the first message by these new citizens is often a little&#8230; "off" if you are used to being on the fediverse.

Allow me to elaborate. The introductory message of Twitter exiles often reads as follows: "Hello all! I am [insert name here], I am going to post messages about [insert multiple topics here]. Who are the people I should follow?".

The message above oozes "Twitter Mentality". To explain what I mean, let me use an analogy.

## The analogy

Twitter is a metropole. Its users all share the same playing field. If you want to participate, you don't talk, you shout. How else are you going to be heard in a crowd of millions? Once you start shouting, quiet people start to listen. This creates a one-to-many dynamic.

The fediverse is a network of well-connected villages. As part of a village, you get to know people. You talk to people because it's less crowded, there's less competition. It's still a network so you can connect to people multiple villages away with the same ease. But the noise is filtered. You are surrounded by people who share a common interest which is the reason you decided to live in that specific village in the first place, but you still get the network effect and communicate with people outside of the village because you want to, because you can. This creates a one-to-one or on a larger scale, many-to-many dynamic.

## What makes a network social?

My argument is that a one-to-many network is not a social network, it's broadcasting. Having "influencers" on a network is, if anything, anti-social. This is where I need to go back to that initial message by the Twitter exile. There's no need to announce you are going to talk about a certain topic, just do it. I will not follow you because you are announcing to post messages about a certain topic. Post a message, let's debate and talk about it and if I am truly interested in your opinion and reasonings, only then will I follow you. And don't follow people because many others do; follow them because you want to, because you get the choice.

This is my opinion and I have talked to many sharing the same sentiment. This many-to-many dynamic is what makes the fediverse appealing. You get a Home timeline where you will find posts from people you follow and actually want to hear from. You get a Local Timeline filled with posts from people you don't always know, but it's the same instance/village, so you already share a common ground. And for when you feel like exploring, you get a Known Network timeline filled with all sorts of posts.

You may argue that building this whole narrative around a simple introductory post is flimsy, and I agree. Not everyone's first post is like that, and even if it were, there are different ways of interpreting the content. But it's something I found people can relate to, it's a bridge between two mentalities, two social structures that I can use to introduce the many advantages of the fediverse.

If you would like actually useful information on starting with the fediverse with Mastodon, please have a look at this [blog post by KevQ](https://kevq.uk/how-does-mastodon-work/).
