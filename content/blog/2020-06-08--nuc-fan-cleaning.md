+++
title = "Friendly reminder to clean your NUC's fan"
slug = "nuc-fan-cleaning"
date = "2020-06-08 13:35:15"

aliases = ["post/nuc-fan-cleaning"]

[taxonomies]
tags = ["short", "homelab"]
+++

[Intel NUCs](https://www.intel.com/content/www/us/en/products/boards-kits/nuc.html) make for some great low-entry-barrier low-power-consumption servers and homelabs. I have three NUCs at home, two of which have played a server role. They span several generations: a 5i3, a 7i7 and a 8i5.

And they all have one thing in common: sooner or later, their fans clog up with dust, they heat up, they make more noise and perform worse.

If you haven't cleaned the fan in a while, your best bet is to open the NUC up and clean the fan and the exhaust.

To prevent having to open a NUC up too often, I bought a few cans of compressed air and regularly blow air through the device. I'm also looking into placing air filters near the air intake.

![NUC cools down when fan is cleaned](/img/blog/nuc_temp_fan_cleaning.png)  
*Can you tell when compressed air was applied to the NUC?*
