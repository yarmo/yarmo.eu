+++
title = "A response to ICANN's refusal to sell .ORG"
slug = "icann-rejects-sale-org"
date = "2020-05-01 09:33:40"

aliases = ["post/icann-rejects-sale-org"]

[taxonomies]
tags = ["short"]
+++

`#100DaysToOffload >> 2020-05-01 >> 007/100`

A response to [ICANN's refusal to sell .ORG](https://www.icann.org/news/blog/icann-board-withholds-consent-for-a-change-of-control-of-the-public-interest-registry-pir) in 3 movements.

My first reaction was sarcastic when I saw the cheer on social media: "look at us celebrating like there's no tomorrow because a non-profit organisation chose to NOT sell a TLD made for non-profit organisations to a for-profit corporation".

But they indeed chose not to. They really chose not to. They didn't do it. The people spoke and the people won. The powers that be got greedy, misread the room and adjusted their path because and only because of the people. A great, great thanks to all who wrote letters to the California Attorney General and made their voices heard online. This is a victory for all.

Today, we celebrate. Unfortunately, tomorrow, we need to think about what happens next. The internet is still under threat. A group of people have full power over what the internet looks like and they have shown us to be untrustworthy. For each domain we buy, we pay an ICANN fee, yet ICANN has made it clear that they do not have our interests at heart. Stay safe outside and vigilant on the web.

If within your possibilities and beliefs, please support the [OpenNIC project](https://www.opennic.org/) (no affiliation, just a fan), a "user-owned and -controlled DNS root offering an alternative to ICANN and the traditional TLD registries".