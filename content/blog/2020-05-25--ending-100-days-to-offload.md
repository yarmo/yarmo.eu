+++
title = "Ending #100DaysToOffload"
slug = "ending-100-days-to-offload"
date = "2020-05-25 16:57:10"

aliases = ["post/ending-100-days-to-offload"]

[taxonomies]
tags = ["short"]
+++

`#100DaysToOffload >> 2020-05-25 >> 025/100`

Today, I'm ending my participation in the #100DaysToOffload challenge at precisely a quarter of the way. I'm happy to have been part of it as it has given me much.

I only had a handful of blog posts when I started my personal website, scattered over a period of multiple months. I didn't write much, I didn't take the time for it and more importantly, I didn't see the point. It was an interesting experience, for sure, but what else? Was it just writing for writing sake?

Along came the #100DaysToOffload challenge. I joined the minute I saw the first toot by Kev and immediately wrote about, well, participating in the challenge.

## Benefits

Twenty-five posts later, I learned a great deal. Forcing myself to post something every day taught me writing doesn't have to be a long and tedious process. Quite the opposite, it forced my perfectionnist brain to settle for "good enough" content.

Posting links to my blog posts (and later, notes) on the fediverse has sparked on several occasions interesting debates involving interesting people with different interesting views. This must have been the most rewarding benefit of all.

I am grateful to have learned this and I will go forth on the path I am now walking, posting regularly about all things that interest me and having eye-opening conversations. I would also have continued the challenge, were it not for a few downsides.

## Drawbacks

First and foremost, the requirement to post every day. I know, I know, it didn't have to be every day. However, skipping every other day would drag this challenge to 200 days and also goes a bit against the whole idea behind it.

I am already mentally exhausted from my recent PhD experience. Although the experience of writing is freeing, there is definitely the possibility of having "too much of a good thing". Not writing every day also gives a feeling of failure as I'm letting myself down for not keeping up. And that is just something I could definitely do without right now.

Posting this much content also dilutes the pool of topics and results in slightly lower quality content. I've talked about this before and is somewhat the purpose of the challenge: just write and publish, quality comes with experience, not from delaying posts for weeks while endlessly fine-tuning every word.

The thing is, I also have this blog for a more serious reason, to showcase my capacity for reasoning and tech skills where my educational background is somewhat lacking. Sure, having done a PhD in Neuroscience is cool but that doesn't tell you (a future employer?) that I have experience with containers and networks and FOSS and… You get the point.

In an unpredictable turn of events, the challenge is now holding me back in a way: I feel guilty when not writing and when I do write, it's often a simpler topic just to get something out there, leaving me with less time to dig into the stuff I now really want to write about.

## In the end

So there you have it. I would love to post every other day and I will. But with no obligations or reasoning. Just because I want to.

I will now dive deeper into the stuff I am passionate about and with more vigor and regularity. And that, I owe to the #100DaysToOffload challenge.