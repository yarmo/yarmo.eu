+++
title = "Start of the Plausible experiment"
slug = "plausible-start"
date = "2020-06-11 12:01:57"

aliases = ["post/plausible-start"]

[taxonomies]
tags = ["short"]
+++

During the roughly 6 months since I started this website, I have not been using any website statistics whatsoever. I did not see the point of it, this website was not designed to gather an audience in any fashion, it was primarily meant to be a permanently-updated online CV. Given that I am leaving academia which I have been preparing for during the last nine years, I figured I could use any means of getting my name out there.

Recently, I have taken an interest in blogging about selfhosting, online privacy and related technical subjects. In an attempt to understand if people see these articles or any other section of my website, I will start an experiment gathering statistics using the privacy-friendly [Plausible](https://plausible.io).

## The Plausible experiment

In a month or so, I will look back at the data gathered and see if anything of interest can be learned. The danger is that when the observation is made some articles perform better than others, the writing process is consequently changed to conform to what the statistics say performs best.

This is not my intention for the simple reason that this blog is not made to target a specific audience but rather to serve as an outlet for things I learn and interest me. If I notice my writing behavior change due to insights gained by statistics, the experiment is ended.

## A comparative experiment
In the near-future, I will also compare what can be learned from a "client-side" statistics solution like [Plausible](https://plausible.io) with what can be learned from a "server-side" statistics solution like [GoAccess](https://goaccess.io).

The reason I am not performing this comparative experiment right now is because both solutions above manage to not support a single common log format. It seems it was decided a month or so ago that [GoAccess should conform to Caddy's format](https://github.com/allinurl/goaccess/issues/1768#issuecomment-629652452) ([separate issue on Caddy's side](https://github.com/caddyserver/caddy/issues/3417#issuecomment-629836804)). Until that happens (or until I figure out a way to parse Caddy's log format in GoAccess), this comparative experiment will have to wait.

---

## Update

The comparative experiment is back on! Thanks to [@AlexMV12](https://fosstodon.org/@AlexMV12) and this [blog post](https://alexmv12.xyz/blog/goaccess_caddy/) he wrote, I now have a working bash script to analyze Caddy's log file. See you in thirty days!
