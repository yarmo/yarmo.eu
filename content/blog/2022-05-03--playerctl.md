+++
title = "playerctl: get currently playing music"
slug = "playerctl"
date = "2022-05-03 14:35:49"

[taxonomies]
tags = ["short", "nuggets", "selfhosting"]
+++

## TLDR

To get information about music currently playing on the computer, run:

```bash
playerctl metadata --all-players --format '{{ status }}: {{ artist }} - {{ title }}'
```

A wide variety of media players are MPRIS-enabled and can be queried using the above command, including media players running in the browser such as [Airsonic](https://github.com/airsonic-advanced/airsonic-advanced).

[playerctl on man.archlinux.org](https://man.archlinux.org/man/community/playerctl/playerctl.1.en)

## Explanation

For a long time, I've been using [Airsonic](https://github.com/airsonic-advanced/airsonic-advanced) as my media player, handy if you are into selfhosted services and despise streaming platforms with a passion.

Since I also do [live streaming](https://yarmo.live) and like to play ambient music in the background, I attempted to build a little "Now playing" widget for on stream.

Sadly, no luck: Airsonic doesn't have an API.

After a lot of trying out different clients and hosting additional services, I settled on syncing my music collection from the NAS to my computer and then playing music through mopidy, as I could then query the currently playing music using MPD:

```bash
# Snippet will be added soon, currently away from computer
```

Not my favorite solution but it works.

In comes this guy [DeavidSedice](https://deavid.wordpress.com/) and he tells me I can get that same information from most existing media players with a single line of bash:

```bash
# The command as he sent it to me
playerctl metadata --all-players --format '{{ status }}: {{ artist }} - {{ title }}               ' | grep Playing | tr '\n' ' '
```

Why did I never hear about this before? A life changer! And it even works with Airsonic running in the browser.

This tool makes it quite trivial to write a bash script that loops the command, writes the output to a text file and have OBS display it on stream.