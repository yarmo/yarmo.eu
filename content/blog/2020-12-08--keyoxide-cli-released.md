+++
title = "Keyoxide CLI released"
slug = "keyoxide-cli-released"
date = "2020-12-08 16:30:00"

aliases = ["post/keyoxide-cli-released"]

[taxonomies]
tags = ["keyoxide"]
+++

Five months ago when I made [keyoxide.org](https://keyoxide.org) public, one
specific request made by quite a few people stood out: we need the ability to
perform the identity verification locally. And given that it was a quite
technical crowd, this meant: we need a command-line interface (CLI).

## The command-line interface

Today, I'm pleased to announce the release of the CLI. Written in Node.js and
published on [Codeberg](https://codeberg.org/keyoxide/cli) under the
[AGPL-v3.0-or-later](https://codeberg.org/keyoxide/cli/src/branch/main/LICENSE)
license, the Keyoxide CLI uses the recently released
[doip.js](https://js.doip.rocks) library and does all the things the Keyoxide
website does, but locally. This means you no longer need to trust the website of
the Keyoxide instance you are using, its maintainer or everything inbetween.

Your machine fetches the keys, parses them locally and then directly requests
the identity proofs from the service providers to verify the identity
claims. Here's a quick tour.

Assuming you already have Node.js installed, first install the CLI:

```bash
npm install -g keyoxide
```

Then go and verify the identity proofs inside a cryptographic key! To get
started, try out the key I use for testing:

```bash
keyoxide verify hkp:test@doip.rocks
```

You should now get the following result:

```
Verification results:
Yarmo Mackenbach (material for test frameworks) <test@doip.rocks>
  ✓ doip.rocks (dns)
```

And there you have it! Whoever generated this key verifiably owns the
[doip.rocks](https://doip.rocks) domain name.

Of course, the CLI can also fetch keys using WKD or get them from Keybase. More
information about these protocols is available on the
[Keyoxide](https://keyoxide.org/) website and in the source code's
[readme](https://codeberg.org/keyoxide/cli) document.

## FOSS FTW

As always, this project is fully open source and I welcome all criticism and
contributions, both issues and PRs. We all stand to benefit from solutions that
are built for and by the people. As governments worldwide push for cryptographic
backdoors, let us all keep using and promote free and open software.

Many thanks to [NLnet](https://nlnet.nl) for supporting me on this journey and
allowing me to focus on these projects while keeping them free from VC funding
and other means of monetization that could compromise the privacy of the
individual.

If you value my efforts and would like to donate, it's possible to do so on the
project's [Liberapay](https://liberapay.com/Keyoxide/) page. Cheers and I'll see
you in the next Keyoxide project update post!
