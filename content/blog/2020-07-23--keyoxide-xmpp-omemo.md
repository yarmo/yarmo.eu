+++
title = "Keyoxide and XMPP + OMEMO"
slug = "keyoxide-xmpp-omemo"
date = "2020-07-23 14:08:02"

aliases = ["post/keyoxide-xmpp-omemo"]

[taxonomies]
tags = ["cryptography", "keyoxide"]
+++

## XMPP

[XMPP](https://xmpp.org/) is an open messaging protocol that not only drives a **thriving secure communication ecosystem for the privacy-minded**, but also handles the messages sent by platforms like WhatsApp and Zoom. Knowingly or not, you have most likely used XMPP at some point in your life.

*For the rest of this post, we will not take into account services like WhatsApp and Zoom as their platforms are closed off from all other platforms even though they use the same XMPP protocol.*

That **ecosystem for the privacy-minded** consists of libraries for developers, server applications for the tech-savvy service providers and clients like [Dino](https://dino.im/) (Linux), [Gajim](https://gajim.org/) (Windows, Mac, Linux) and [Conversations](https://conversations.im/) (Android) for everyone.

Because there is no single server or client to rule them all, we call this is a *decentralized* network. I could use a different server and a different client than you do, but we would still be able to communicate with each other. Also, any server or client could cease to exist the next day without impacting the rest of the network.

## Care to join the XMPP ecosystem?

Joining the XMPP ecosystem is as simple as making an account on a server and logging in using any XMPP-compatible client. But which server? Which client?

While not the focus of this post, here is a [list provided by 404.city](https://xmpp-servers.404.city/) and a [list provided by jabber.at](https://list.jabber.at/) of XMPP servers.

Notable mention for [404.city](https://404.city/) itself. Not sponsored. Just a fan.

With regards to clients, the three mentioned above should get you started. Need a different client? Have a look at this [list provided by xmpp.org](https://xmpp.org/software/clients.html).

## End-to-end encryption: OMEMO

XMPP communication can be end-to-end encrypted with [OMEMO](https://conversations.im/omemo/) ([XEP-0384](https://xmpp.org/extensions/xep-0384.html)), the easiest and most common of [XMPP-compatible end-to-end encryption schemes](https://wiki.404.city/en/XMPP_client_encryption). Verifying OMEMO fingerprints is essential to trust your communication and keep it safe from Man-in-the-Middle attacks.

Each XMPP client you use will have its own OMEMO key, the content of which remains secured on your device but a "fingerprint" of which can be made public without a problem. These fingerprints are used to identify the different clients that have logged in on your XMPP account.

If you wish to secure your communication with OMEMO, make sure to choose a [client with full support on this website](https://omemo.top/).

## OMEMO and trust

When you talk with someone over XMPP and you want to guarantee all communication is secured, it is recommended to use a different form of communication to compare and trust each others fingerprints. Ideally, you would meet in person and scan QR codes, a handy function of the **Conversations** app.

## XMPP identity proofs and Keyoxide

As you can see, trusting OMEMO keys is an essential step in the process of ensuring communication is secure. Fortunately, [Keyoxide](https://keyoxide.org) can assist you in that process.

As of [version 0.4](https://codeberg.org/keyoxide/web/releases/tag/0.4.0), Keyoxide generates QR codes for all **verified** XMPP accounts it detects. This makes it easy to add new contacts if your [XMPP identity proof](https://keyoxide.org/guides/xmpp) looks like this:

```
proof@metacode.biz=xmpp:username@domain.org
```

Scan the resulting QR code on a Keyoxide profile page in the **Conversations** app and the contact is added. But the OMEMO keys are not yet trusted. Let's solve that!

## Integrating OMEMO in the XMPP identity proof

It is also possible to add a more advanced [XMPP identity proof](https://keyoxide.org/guides/xmpp) to your OpenPGP key that includes the OMEMO fingerprints:

```
proof@metacode.biz=xmpp:user@domain.org?omemo-sid-123456789=A1B2C3D4E5F6G7H8I9...
```

Obtaining the correct URI for the proof can be difficult when doing so manually. Fortunately, this can be assisted by the **Conversations** app. As you can tell, using the **Conversations** app brings a ton of advantages.

In the main menu of that app, press **Manage accounts > [your account] > Share > Share as XMPP URI** and add the resulting URI to your key using [this Keyoxide guide](https://keyoxide.org/guides/xmpp).

Scan the resulting QR code on a Keyoxide profile page and not only is the contact added, their OMEMO fingerprints are also fully trusted and verified.

## Why trust the Keyoxide identity proof?

Anyone can add any XMPP proof to their OpenPGP key, whether they own it or not. So why trust the identity proof on Keyoxide?

**STEP 1** The QR code is only shown if a XMPP identity proof is verified. Verifying a XMPP account requires the holder of said account to add a small line of code to their XMPP bio [as described in this guide](https://keyoxide.org/guides/xmpp). Only a person with access to both the OpenPGP private key and the XMPP account can verify that XMPP account.

**STEP 2** While Keyoxide assists as much as possible with trusting the right proofs, a critical mind is always an asset when dealing with trusting online identities, especially when securing your communication. Do you recognize any other proofs on this person's profile page? Is this proof verified? If so, you can safely assume that the person who holds the OpenPGP key also has access to this "other proof".

Combining the two steps above, you can trust that you are talking to the right person and verifying the right OMEMO fingerprints.

## Example

You are reading this post on [yarmo.eu](https://yarmo.eu). Whether or not you trust me, I'm telling you that my OpenPGP fingerprint is:

```
9f0048ac0b23301e1f77e994909f6bd6f80f485d
```

So you visit [keyoxide.org/9f0048ac0b23301e1f77e994909f6bd6f80f485d](https://keyoxide.org/9f0048ac0b23301e1f77e994909f6bd6f80f485d). Indeed, whoever holds the key with that fingerprint also owns the [yarmo.eu](https://yarmo.eu) domain.

Now, you scroll down until you reach the XMPP proof for **yarmo@404.city**. You read that the XMPP account is verified. Ergo, whoever holds the key with that fingerprint also has access to that XMPP account.

Final conclusion: whoever owns the [yarmo.eu](https://yarmo.eu) domain also has access to the **yarmo@404.city** XMPP account. If you wish to talk with me securely, scan the QR code and be certain that you have just added me as a contact, and that are you verifying the right OMEMO fingerprints to ensure secure and fully encrypted communication between us.
