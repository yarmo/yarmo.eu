+++
title = "The Post-MomentJS Era"
slug = "post-momentjs-era"
date = "2020-09-15 09:46:54"

aliases = ["post/post-momentjs-era"]

[taxonomies]
tags = ["short"]
+++

## The Post-MomentJS Era

According to their [own documentation](https://momentjs.com/docs/#/-project-status/), new projects should no longer use [MomentJS](https://momentjs.com), mentioning its hefty size and its outdated architecture as the principal reasons behind this statement.

Although there are new libraries that they do recommend, we also have a different solution nowadays: no library.

Using ECMAScript [Intl](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl), we can go a very long way formatting dates without dependencies.

```js
Intl.DateTimeFormat("en", {
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
    hour12: false,
    timeZone: "CET"
}).format(new Date());
// -> September 15, 2020, 09:41
```

```js
Intl.DateTimeFormat(navigator.language, {
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
    hour12: false,
    timeZone: "CET"
}).format(new Date());
// -> 15 september 2020 09:41
// In dutch!
```

ISO formatting with `Intl` is tricky. But we don't need it.

```js
new Date().toISOString();
// -> 2020-09-15T07:41:41.148Z
```

```js
new Date().toISOString().split("T")[0];
// -> 2020-09-15
```

Have a look at the [MDN docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl) for more information.

Even though `Intl` and `Date` are viable options, the MomentJS developers recommend a few libraries to help with some browser inconsistencies. Make sure to [read the MomentJS post](https://momentjs.com/docs/#/-project-status/) for all the pros and cons.
