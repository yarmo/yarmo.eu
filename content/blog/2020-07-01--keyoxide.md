+++
title = "Launching Keyoxide.org"
slug = "keyoxide"
date = "2020-07-01 12:00:00"

aliases = ["post/keyoxide"]

[taxonomies]
tags = ["project", "cryptography", "identity", "decentralization", "keyoxide"]

[extra]
discussion = [
  "https://lobste.rs/s/2ab63g/launching_keyoxide_org",
  "https://tildes.net/~comp/q7o/launching_keyoxide",
  "https://news.ycombinator.com/item?id=23699821"
]
+++

Today, I'm excited to launch [Keyoxide.org](https://keyoxide.org), the lightweight and FOSS solution to make basic cryptography operations accessible to regular humans.

## What is Keyoxide.org?

[Keyoxide.org](https://keyoxide.org) offers easy encryption, signature verification and decentralized identity proof verification based on PGP keys while demanding little in-depth knowledge about the underlying encryption program from its users.

This project aims to offer comparable functionality as services like [Keybase](https://keybase.io) while reducing friction and being more open.

The project is MIT licensed, uses [openpgpjs](https://github.com/openpgpjs/openpgpjs) and is hosted on [Codeberg](https://codeberg.org/yarmo/keyoxide).

## Why only encryption and signature verification?

These are the operations that are available when only having access to public keys instead of private keys. If you wish to decrypt messages and sign them, you need a keypair. If you have a keypair, you probably have the knowledge to use dedicated tools like the CLI or Kleopatra. And if you do, you probably won't be using [Keyoxide.org](https://keyoxide.org) directly yourself.

Indeed, if you possess a PGP keypair, [Keyoxide.org](https://keyoxide.org) is the tool you send to others to interact with your public key more easily. Allow them to encrypt a message for you, to verify one of your signatures, to verify your online identities using decentralized proofs.

## What are those decentralized identity proofs you keep mentioning?

You know how Keybase allows you to prove you have control over accounts on certain websites and services? A great function! Fortunately for you, this function can be even better and more secure by using [decentralized OpenPGP identity proofs](https://keyoxide.org/guides/openpgp-proofs). [Keyoxide.org](https://keyoxide.org) will prove your identity on multiple platforms at the same time and yet, you are not required to make an account to use this function. How is that possible?

Well, it's called *decentralized* for a reason: [Keyoxide.org](https://keyoxide.org) doesn't hold your proofs, your key does! Any software that can access your public key can verify these proofs for anyone. When better tooling comes around, you could verify those proofs using a mobile app, using a command-line utility, you name it. No single service holds your proof, only you do, stored inside your keypair.

I have written a [guide](https://keyoxide.org/guides) on how to add a proof for every platform currently supported by this website: [domains](https://keyoxide.org/guides/dns), [Lobste.rs](https://keyoxide.org/guides/lobsters), [Twitter](https://keyoxide.org/guides/twitter), [Github](https://keyoxide.org/guides/github), a [bunch more](https://keyoxide.org/guides) and work is in progress to support even more still. Is your beloved service not in the list? [Open an issue or make a PR](https://codeberg.org/yarmo/keyoxide)! Free open-source software FTW!

Oh, that reminds me, any [Mastodon](https://keyoxide.org/guides/mastodon) instance can be used to prove your identity. Yes, [any](https://github.com/keybase/keybase-issues/issues/3385).

## So how does it compare to Keybase?

There's a more complete [guide on the Keyoxide website](https://keyoxide.org/guides/feature-comparison-keybase), but in a nutshell:

- more privacy-friendly by not forcing you to create an account and handing over data
- more secure by not asking you to trust the service with your private keys
- open-source servers ([a must](https://github.com/keybase/client/issues/24105))
- encrypt/verify with every public key accessible on the internet, not just those that have been uploaded to a proprietary server
- almost all processing is done in the browser, no data is sent to servers*
- no vendor lock-in
- selfhostable

\* Only exception is decentralized identity proof verification: some service providers do not have the correct CORS headers (like Reddit) or require APIs (like Twitter). In these rare cases, simple PHP scripts (also open-source) run the proof verification instead.

## Can I get an account?

No. [Keyoxide.org](https://keyoxide.org) doesn't need your data on its servers. There are already several ways of exposing public keys on the internet, including [web key directory](https://keyoxide.org/guides/web-key-directory) (WKD) and dedicated servers like [keys.openpgp.org](https://keys.openpgp.org). Let's use those instead of making yet another service where you need to upload your keys to.

## Can I get a profile page then?

Yes! Append your PGP fingerprint or WKD id to the URL and there it is!

Want an example? Here's  my profile at  
[https://keyoxide.org/9f0048ac0b23301e1f77e994909f6bd6f80f485d](https://keyoxide.org/9f0048ac0b23301e1f77e994909f6bd6f80f485d).

Now you know what accounts on various services are mine, where to follow me if you wish to get updates on the project and if you wish to send me an encrypted message, that's also just two clicks away.

## What about my private keys?

Don't upload your private keys to the internet, period. If a service wants your private keys on their (proprietary) servers, say no.

## You said selfhostable?

Well, yes! It's not a fully supported use case just yet, but the browser does all the processing, the server is mostly just there to deliver the files to the user to perform the operations. [Grab the code](https://codeberg.org/yarmo/keyoxide) and put it on your own PHP server!

## Any closing words?

I built this to provide better tooling around modern-day encryption programs and reduce the friction for less tech-savvy people when interacting with public keys.

For those who wish to use encryption programs beyond OpenPGP, [let's talk about this](https://codeberg.org/yarmo/keyoxide/issues). Keyoxide doesn't have any reference to PGP in its name for a reason: it could serve as a platform for easy interaction with any public key, no matter the underlying encryption program.

And above all, I hope you see the same benefit and potential in [Keyoxide.org](https://keyoxide.org) as I do and would like to see it grow as an open and accessible platform to push forward the democratization of online privacy and security.

Privacy is not a luxury.

Many thanks to [Wiktor](https://metacode.biz/@wiktor) for helping with the decentralized identity proofs.
