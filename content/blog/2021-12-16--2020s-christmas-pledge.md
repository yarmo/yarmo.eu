+++
title = "My 2020s Christmas Gift Pledge"
slug = "2020s-christmas-gift-pledge"
date = "2021-12-16 11:22:57"

aliases = ["xmas"]

[taxonomies]
tags = ["personal-development", "christmas"]
+++

## Christmas and consumerism

Christmas in the 2020s = consumerism. You know it, I know it. I am not even going to give an introduction on the subject. Even the Pope has condemned the commercialization of Christmas.

So, you're either here because you want to read the blog post, or you haven't received from me a Christmas gift that lives up to our society's norms, asked me "what gives?" and I sent you here.

## Me the Consumer

I live in a consumer society ergo I am a consumer. Be that as it may, I won't be your typical consumer though.

I turn off the radio when ads come on. I look away from a TV screen during ad breaks. If I still see an ad of a product somewhere, I am instantly less prone to using it or recommending it.

If I see a single billboard promoting some TV show, I'll instantly know the people behind it don't have enough faith in the show that it would organically grow an audience and thus feel the need to shove it down our throats instead. I'll pass on said show.

I don't use Amazon. I condemn its use.

I try and support local to the best of my abilities.

Given all these idiosyncracies and more, you do not expect me to do Christmas like a well-behaving consumer, do you?

## Doing things differently

So I want to write a pledge, a set of rules to live by and guide me through the yearly burden that is Christmas, so that whenever I am in doubt about a gift-related decision, I can just read the pledge and know what to do.

I want to make the pledge last a decade so that I won't stray from my convictions and my rejection of consumerism. I can abandon the pledge in the future but that would require me to write a new one, forcing me to re-evaluate everything in the current pledge.

I also don't expect Christmas to become less commercial any time soon so I'll need this pledge for the foreseeable future anyway.

I want to make my pledge public so that I can send people to it whenever my gift-related decisions are being questioned.

## Message to Future Me

Future Me, if you're here because peer pressure is causing you to doubt the pledge, don't. Just don't.  
— 2021 Me

## The Pledge on Giving Gifts

My gifts will not be bought if possible. I will attempt to make something personalized, thoughtful and unique using my hands. It can be big, small, silly, helpful.

My gifts are disposable. Receiving a gift is a burden, therefore no hard feelings if you decide to get rid of it.

My gifts can be ephemeral. I'd rather show you my appreciation of our personal connection by spending quality time together than giving you something. An experience is worth more than an object.

The rules above aren't always suitable or desirable depending on the receiver. In case a gift is bought:

My gifts will not contain plastic. If unavoidable, it will contain minimal plastic wrapping. Objects made entirely of plastic are a no-no. The only exception: custom-made items containing plastic from a 3D printer (while still plastic, it wasn't mass-produced).

My gifts will not be tech, especially surveillance technology. The only exception: something I built myself using an arduino or a single-board-computer like a raspberry pi.

My gifts will never be obtained through Amazon. Some underpaid Amazon worker would have needed to work extra shifts and pee in a bottle to make sure the gift arrives on time. No gift of mine will ever require corporation-organized slavery to be given. Or, for that matter, any slavery of any kind.

## The Pledge on Receiving Gifts

I do not wish to receive bought gifts. Let's hang out for a bit and do something we both enjoy. If you truly feel an urge to give something tangible, write a poem, make a drawing, bake cookies, print out a picture of us and put it in a (non-plastic) frame.

Evidently, this isn't always a realistic expectation. So, a few more guidelines:

I will most likely be polite and accept a bought gift but please do not take it as a given that I will use the gift or, for that matter, keep it. It will depend. One of my ambitions this decade is to live smaller and possess less. Receiving stuff I don't want or need is counterproductive to said ambition.

An exception: surveillance technology. Any gift made by Big Tech that contains microphones, cameras or other tracking capabilities will be unceremoniously rejected on the spot.

Another exception: anything that goes against any of my convictions. Think objects made entirely out of plastic, objects that could cause pollution or waste of resources, objects made through corporation-organized slavery.

If a gift was obtained through Amazon, you and I are going to have a chat.

## Conclusion

Hope you enjoyed the read and—if you know me personally—better understand how I think about Christmas and act when it comes to gifts.