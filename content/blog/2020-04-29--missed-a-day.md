+++
title = "Missed a day"
slug = "missed-a-day"
date = "2020-04-29 09:02:57"

aliases = ["post/missed-a-day"]

[taxonomies]
tags = ["short"]
+++

`#100DaysToOffload >> 2020-04-29 >> 004/100`

Well, that was fast. I missed my first day in the #100DaysToOffload challenge. I am not one to make up excuses and reasons why this has happened.

Though I am not planning to share the layout of my entire day yesterday in this blog post, I will write a little bit about an issue I have been facing lately: memory problems. After talking with experts, this is apparently a common issue people face after prolonged exposure to stressful situations. As a reference, I never had problems remembering things before the PhD, sure my memory was not the best out there, but it served me well. Nowadays, I do tend to forget some things on a daily basis unless I write them down immediately. Well, I will still forget them but at least I'll have an indelible reminder. I remembered at multiple occasions yesterday to write a blog post, but I kept forgetting it a bit later and I didn't make a note of it, so&hellip;

I cannot wait for this to be over. Until then, I will try something new to help me specifically with #100DaysToOffload: I will leave a fully charged Thinkpad by my bedside in the evening, and first thing in the morning, I will write my blog post for that day.

Let's try that :)