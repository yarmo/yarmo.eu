+++
title = "Yarmo's blog"
description = ""
weight = 10

template = "blog-section.html"
page_template = "blog-page.html"

sort_by = "date"

paginate_by = 0
paginate_path = "page"
paginate_reversed = false

generate_feed = true

aliases = ['post']

[extra]
+++