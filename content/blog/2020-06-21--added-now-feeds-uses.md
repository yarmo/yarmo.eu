+++
title = "Added /now, /feeds, /uses"
slug = "added-now-feeds-uses"
date = "2020-06-21 00:54:10"
update = "2021-11-01 17:28:42"

aliases = ["post/added-now-feeds-uses"]

[taxonomies]
tags = ["short", "meta"]
+++

My website now has dedicated [/now](/now), <span class="line-through">/feeds and /uses pages</span>, inspired by several trends in blogging. A new [/friends](/friends) (or something similar) will follow soon!
