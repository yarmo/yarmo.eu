+++
title = "Yarmo's reads"
+++

## Currently reading

- **All Quiet On The Western Front** by Erich Maria Remarque (EN)
- **Factfulness** by Hans Rosling (EN)
- **How Big Things Get Done** by Bent Flyvbjerg & Dan Gardner (EN)
- **The Notebook: A History of Thinking on Paper** by Roland Allen (EN)

## To read

- **The Age of Surveillance Capitalism** by Shoshana Zuboff (EN)
- **Algorithms to Live By** by Brian Christian (EN)
- **L'art d'avoir toujours raison** by Arthur Schopenhauer (FR)
- **The Dark Tower** series by Stephen King (EN)
- **Les expressions françaises pour les nuls** by Marie-Dominique Porée (FR)
- **O Guia do Mochileiro das Galáxias** by Douglas Adams (PT)
- **Hyperfocus** by Chris Bailey (EN)
- **The Lord of the Rings** series by J.R.R. Tolkien (EN)
- **Légendes Basques** by Wentworth Webster (FR)
- **Neuromancer** (Sprawl series) by William Gibson (EN)
- **Norte** by [Oli Maia](https://olimaia.net/) (thanks for the signed book, my friend!) (PT)
- **Other Minds: The Octopus and the Evolution of Intelligent Life** by Peter Godfrey-Smith (EN)
- **The Righteous Mind: Why Good People Are Divided by Politics and Religion** by Jonathan Haidt (EN)
- **The Three-Body Problem** by Liu Cixin (EN)

## Must reads!

### Fiction

- **1984** by George Orwell (EN)
- **Animal Farm** by George Orwell (EN)
- **Dune** by Frank Herbert (EN)
- **Fahrenheit 451** by Ray Bradbury (EN)
- **Little Brother** by Cory Doctorow (EN)

### Non-fiction

- **The Attention Merchants: The Epic Scramble to Get Inside Our Heads** by Tim Wu (EN)
- **Dating Fatigue: Amours et solitudes dans les années (20)20** by Judith Duportail (FR)
- **Deep Work: Rules for Focused Success in a Distracted World** by Cal Newport (EN)
- **Essentialism: The Disciplined Pursuit of Less** by Greg Mckeown (EN)
- **Notes on Nationalism** by George Orwell (EN)

## Recommended reads

### Fiction

- **The 100-Year-Old Man Who Climbed Out the Window and Disappeared** by Jonas Jonasson (EN)
- **Brave New World** by Aldous Huxley (EN)
- **Do Androids Dream of Electric Sheep?** by Philip K. Dick (EN)
- **The Hitchhiker's Guide to the Galaxy** series by Douglas Adams (EN)
    - **The Hitchhiker's Guide to the Galaxy** by Douglas Adams (EN)
    - **The Restaurant at the End of the Universe** by Douglas Adams (EN)
    - **Life, the Universe and Everything** by Douglas Adams (EN)
    - **So Long, and Thanks for All the Fish** by Douglas Adams (EN)
    - **Mostly Harmless** by Douglas Adams (EN)
-  **World War Z: An Oral History of the Zombie War** by Max Brooks (EN)

### Non-fiction

- **L'Amour sous algorithme** by Judith Duportail (FR)
- **The Big Short: Inside the Doomsday Machine** by Michael Lewis (EN)
- **De Bullet Journal Methode** by Ryder Carroll (NL)
- **Command and Control** by Eric Schlosser (EN)
- **Double Entry: How the Merchants of Venice Created Modern Finance** by Jane Gleeson-White (EN)
- **The Dictator's Handbook: Why Bad Behavior is Almost Always Good Politics** by Bruce Bueno de Mesquita (EN)
- **The Everything Store: Jeff Bezos and the Age of Amazon** by Brad Stone (EN)
- **IBM and the Holocaust** by Edwin Black (EN)
- **LikeWar: The Weaponization of Social Media** by P.W. Singer (EN)
- **Het is oorlog maar niemand die het ziet** by Huib Modderkolk (NL)
- **Permanent Record** by Edward Snowden (EN)
- **An Ugly Truth: Inside Facebook's Battle for Domination** by Sheera Frenkel (EN)

## Other reads

### Fiction

- **Children of Dune** by Frank Herbert (EN)
- **The Collected Stories of Philip K. Dick** by Philip K. Dick (EN)
- **Deception Point** by Dan Brown (EN)
- **Digital Fortress** by Dan Brown (EN)
- **Dune Messiah** by Frank Herbert (EN)
- **Halo** series (EN)
    - **The Fall of Reach** by Eric Nylund (EN)
    - **First Strike** by Eric Nylund (EN)
    - **Ghosts of Onyx** by Jonathan Davis (EN)
    - **Halo: Contact Harvest** by Josepth Staten (EN)
    - **Halo: The Cole Protocol** by Tobias S. Buckell (EN)
- **Harry Potter** series by J.K. Rowling (EN)
    - **Harry Potter and the Philosopher's Stone** by J.K. Rowling (EN)
    - **Harry Potter and the Chamber of Secrets** by J.K. Rowling (EN)
    - **Harry Potter and the Prisoner of Azkaban** by J.K. Rowling (EN)
    - **Harry Potter and the Goblet of Fire** by J.K. Rowling (EN)
    - **Harry Potter and the Order of the Phoenix** by J.K. Rowling (EN)
    - **Harry Potter and the Half Blood Prince** by J.K. Rowling (EN)
    - **Harry Potter and the Deathly Hallows** by J.K. Rowling (EN)
- **Lord of the Flies** by William Golding (EN)
- **The Maze Runner** series by James Deshner (EN)
    - **The Maze Runner** by James Deshner (EN)
    - **The Scorch Trials** by James Deshner (EN)
    - **The Death Cure** by James Deshner (EN)
- **Robert Langdon** series by Dan Brown (EN)
    - **Angels & Demons** by Dan Brown (EN)
    - **The Da Vinci Code** by Dan Brown (EN)
    - **The Lost Symbol** by Dan Brown (EN)
    - **Inferno** by Dan Brown (EN)
    - **Origin** by Dan Brown (EN)
- **TikTok Boom: China's Dynamite App and the Superpower Race for Social Media** by Chris Stokel-Walker (EN)

### Non-fiction

- **Attack of the 50 Foot Blockchain: Bitcoin, Blockchain, Ethereum & Smart Contracts** by David Gerard (EN)
- **Battle of the Atlantic** by Marc Milner (EN)
- **Hatching Twitter: A True Story of Money, Power, Friendship, and Betrayal** by Nick Bilton (EN)
- **Je Hebt Wél Iets te Verbergen** by Maurits Marzijn, Dimitri Tokmetzis (NL)
- **Midnight in Chernobyl: The Untold Story of the World's Greatest Nuclear Disaster** by Adam Higginbotham (EN)
- **Petit guide à l'usage des gens intelligents qui ne se trouvent pas très doués** by Béatrice Millêtre (FR)
- **Petit manifeste de la masturbation féminine** by Dr Gaudette Loiseau (FR)
- **Radicalized** by Cory Doctorow (EN)
- **Zucked: Waking Up to the Facebook Catastrophe** by Roger McNamee (EN)