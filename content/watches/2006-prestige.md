+++
title = "The Prestige"
date = "2006-10-17"

[extra]
type = "movie"
rating = 4.5
+++

Excellent portrayal of destructive rivalry.