+++
title = "Mad Max: Fury Road"
date = "2015-05-13"

[extra]
type = "movie"
rating = 5
+++

A masterclass in visual storytelling and visual effects.

<!-- more -->

Within fifteen minutes, you already know everything you need about this post-apocalyptic world: the Citadel is a dictature with a stratified society and sufficient natural resources entirely controlled by the powers that be. It trades its produce with Gastown — which likely has access to an oil refinery facility — and the Bullet Farm — which likely has access to iron ore and a metallurgical facility.

No flashbacks needed. No voice over. No forced exposition.

This movie knows it's a visual medium and acts accordingly.