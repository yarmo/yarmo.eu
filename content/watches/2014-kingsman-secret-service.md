+++
title = "Kingsman: The Secret Service"
date = "2014-12-13"

[extra]
type = "movie"
rating = 4
+++

Funny goofy spy comedy with a moronic antagonist.

<!-- more -->

Don't get me wrong, I'm all for an evil masterplan that saves the planet. But there are a couple of flaws with it, not the least the fact that the evil genius needs to keep his hand on the panel to maintain the signal active. If he gets distracted for a second, the evil masterplan falls apart. What the…

Other than that, pretty well written and performed. Characters make mostly sensical decisions and show actual development. The church scene is pure art.