+++
title = "Interstellar"
date = "2014-11-05"

[extra]
type = "movie"
rating = 5
+++

A masterpiece of science fiction and character building.