+++
title = "Kingsman: The Golden Circle"
date = "2017-09-18"

[extra]
type = "movie"
rating = 2
+++

Why write an intricate story when there's "convenience"?

<!-- more -->

It's a pretty moronic movie where stuff just happens conveniently to advance the plot. Where the first movie had a balance between different types of jokes, this movie only features lowbrow humor intended for children hitting puberty.