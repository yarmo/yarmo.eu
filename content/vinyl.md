+++
title = "Yarmo's vinyl collection"
+++

| Artist              | Title                     | Year |
|:--------------------|:--------------------------|:-----|
| Andrew Lloyd Webber | Jesus Christ Superstar    | 1970 |
| The Antlers         | Hospice                   | 2009 |
| Arcade Fire         | Everything Now            | 2017 |
| Arcade Fire         | Funeral                   | 2004 |
| The Black Keys      | "Let's Rock"              | 2019 |
| Jean-Michel Jarre   | Équinoxe                  | 1978 |
| Jeff Wayne          | The War Of The Worlds     | 1978 |
| Mike Oldfield       | Tubular Bells             | 1973 |
| Mike Oldfield       | Ommadawn                  | 1975 |
| Miles Davis         | Conception                | 1973 |
| The National        | Sleep Well Beast          | 2017 |
| Nina Simone         | High Priestess Of Soul    | 1967 |
| Nina Simone         | I Put A Spell On You      | 1965 |
| Pink Floyd          | The Dark Side Of The Moon | 1972 |
| Pink Floyd          | The Wall                  | 1979 |
| Pink Floyd          | Relics                    | 1971 |
| Tame Impala         | Currents                  | 2015 |
| Various Artists     | Baby Driver OST           | 2017 |
