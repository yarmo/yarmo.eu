+++
title = "Yarmo uses"
+++

## Developer tools

- Code editor: [helix](https://helix-editor.com/)
- Code editor: [nano](https://www.nano-editor.org/)
- Code editor: [VSCodium](https://vscodium.com/)
- Editor theme: [Rose Pine](https://rosepinetheme.com/)
- Programming font: [Pragmasevka](https://github.com/shytikov/pragmasevka)
- Programming font: [mononoki](https://madmalik.github.io/mononoki/)
- API testing: [bruno](https://www.usebruno.com/)
- Code forge: [codeberg.org](https://codeberg.org/)

## Laptop

- Model: Tuxedo Pulse 14
- CPU: AMD Ryzen 7 7840HS (16) @ 6.68GHz
- RAM: 25 GiB
- OS: NixOS
- WM: AwesomeWM
- Keyboard: Keychron Q8
- Mouse: Kensington Orbit Fusion

## Desktop

- Custom built
- CPU: AMD Ryzen 5 3600
- GPU: AMD Radeon RX 580
- RAM: 16 GB
- OS: EndeavourOS
- WM: bspwm
- Keyboard: Anne Pro 2

## Backup Laptop

- Model: Lenovo ThinkPad x201i
- CPU: Intel i3 M 330 (4) @ 2.133GHz
- RAM: 4 GB
- OS: EndeavourOS
- WM: sway
- Keyboard: Keyboardio Atreus

## Phone

- Model: OnePlus 5T
- OS: LineageOS

## Other tech

- E-reader: Kobo Libra H20
- OpenPGP smartcard: YubiKey 5 NFC (USB-A)
- Password manager: Bitwarden, KeePass

## Stationary

- Bullet journal: Leuchtturm1917 A5 dotted
- Fountain pen: Lamy AL-star
- Pen: Sakura Pigma Micron
